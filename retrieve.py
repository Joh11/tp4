import pymatgen as mg
from pymatgen.ext.matproj import MPRester
import matplotlib.pyplot as plt
import numpy as np

api_key = "CfwsHtbTJWgDmlvn"

chem_syss = ["C", "C-H"]

def load_data():
    # Get the entries and structures
    with MPRester(api_key) as m:
        # structs = [m.get_structure_by_material_id(id) for id in ids]
        entries = []
        for chem_sys in chem_syss:
            entries += m.get_entries_in_chemsys(chem_sys)
            
        material_ids = [entry.entry_id for entry in entries]
        structs = [m.get_structure_by_material_id(material_id) for material_id in material_ids]
        return entries, structs

def positions_and_atomic_numbers(struct):
    """Returns a tuple of arrays for the position and atomic number for
    each atom in the unit cell.

    Returns: array((N, 3)), array((N,))"""
    # Make a big sphere instead
    r = 20
    sites = [s[0] for s in struct.get_sites_in_sphere([0, 0, 0], r)]
    coords = [site.coords for site in sites]
    coords = np.row_stack(coords)
    
    comps = [site.species for site in sites]
    elems = [comp.elements for comp in comps]

    if not all([len(elem) == 1 for elem in elems]): # If any site is occupied by more than one atom, we stop
        # TODO make a better behaviour
        raise Exception("More than one atom per site")

    species = np.array([elem[0].number for elem in elems])

    return coords, species

def energy(entry):
    return entry.energy



entries, structs = load_data()

energies = [energy(e) for e in entries]
pz = [positions_and_atomic_numbers(s) for s in structs]
pos = [x[0] for x in pz]
atomic_numbers = [x[1] for x in pz]

np.savez("data/coords.npz", *pos)
np.savez("data/type.npz", *atomic_numbers)
np.savez("data/energy.npz", *energies)

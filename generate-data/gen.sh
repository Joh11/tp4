#!/bin/bash

WORKDIR=/scratch/felisaz/

cd $WORKDIR

# Virtual environment
source ~/ve/bin/activate

# Setup the temporary folder
rm -rdf tmp/
mkdir tmp

# Generate graphene like materials
# Split into 10 jobs for now
python3 ~/tp4/generate-data/generate.py
echo "Materials generated..."

# Compute their energy
# For each material run another job id

python3 ~/tp4/generate-data/compute_energy.py
echo "Energies computed ..."

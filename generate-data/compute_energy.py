"""Script to compute the energy for each configuration stored in the
given npz files

"""

import numpy as np
import sys
import os
import time

infile = "tmp/data"
outfile = "tmp/energy"
workdir= "tmp/"

cfgdir = "./"#"~/tp4/generate-data/"
vasp_bin = "~/src/vasp.5.4.4/bin/vasp_std"

data = np.load(infile + ".npz")
basis = np.load(infile + "_basis.npz")

def write_poscar(path, data, basis):
    with open(path, 'w') as f:
        f.writelines(["Graphene like material\n", 
                      "1.0\n"])
        f.writelines(["{} {} {}\n".format(x[0], x[1], x[2]) for x in basis])
        f.writelines(["{}\n".format(data.shape[0]), "cartesian\n"])
        f.writelines(["{} {} {}\n".format(x[0], x[1], x[2]) for x in data])

def write_script(path, index):
    with open(path, 'w') as f:
        f.write("""#!/bin/bash -l
#
#SBATCH --exclusive
#SBATCH --time=0:30:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node=14
#SBATCH --job-name=vasp-graphene-{}
#SBATCH --output=./output
#SBATCH --error=./errormsg
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export MV2_ENABLE_AFFINITY=0
echo "The current job ID is $SLURM_JOB_ID"
echo "Running on $SLURM_JOB_NUM_NODES nodes:"
echo $SLURM_JOB_NODELIST
echo "Using $SLURM_NTASKS_PER_NODE tasks per node"
echo "A total of $SLURM_NTASKS tasks is used"

module purge
module load intel
module load intel-mpi

srun {}""".format(index, vasp_bin))

energies = []
    
for i in basis.keys(): # for each "kind" of material (monolayer, bilayer ...)
    for j in range(len(data[i])): # for each material
        prefix=workdir+"{}-{}/".format(i, j)
        # Make the new directory
        os.system("mkdir {}".format(prefix))
        # Import the config files
        for x in ["KPOINTS", "POTCAR", "INCAR"]:
            os.system("cp {}{} {}{}".format(cfgdir, x, prefix, x))
        # Generate the POSCAR
        write_poscar(prefix+"POSCAR", data[i][j], basis[i])

        # Generate the bash script
        write_script(prefix+"vasp.sh", "{}-{}".format(i, j))

        # And run it
        os.system("sbatch {}vasp.sh".format(prefix))
        time.sleep(0.1)

np.savez(outfile + ".npz", energies)

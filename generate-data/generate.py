import numpy as np
import matplotlib.pyplot as plt
import pymatgen as mg
from math import sqrt, ceil
import sys

# Parameters
Nsamples_mono = 1 #1000
Nsamples_bilayer = 0 #1000

Nscale_mono = 4
Nscale_bilayer = 4

perturb_ratio = 0.1

outfile = "tmp/data"

def plot2D(struct):
    coords = np.row_stack([x[0].coords for x in struct.get_sites_in_sphere([0, 0, 0], 10)])
    fst = coords[coords[:, 2] <= 0.5][:, 0:2]
    snd = coords[coords[:, 2] > 0.5][:, 0:2]

    plt.figure()
    plt.scatter(fst[:, 0], fst[:, 1])
    plt.scatter(snd[:, 0], snd[:, 1])
    plt.show()

def copy_and_perturb(struct, distance):
    s = struct.copy()
    s.perturb(distance)
    return s
    
Catom = mg.Element("C")

# everything is in Angstroms
a = 2.46 # source : wikipedia Graphene page
c = 25 # just a big number
interlayer_dist = 3.4 # according to https://arxiv.org/pdf/1311.2164.pdf

# build a hexagonal lattice
lattice = mg.Lattice([  [a * 3 / 2, a * sqrt(3) / 2, 0]
                      , [0        , a * sqrt(3)    , 0]
                      , [0        , 0              , c]])
monolayer = mg.Structure(lattice, 2 * [Catom], [[0, 0, c / 2], [a, 0, c / 2]], coords_are_cartesian=True)
bilayerAB = mg.Structure(lattice, 4 *[Catom], [[0        , 0              , c / 2],
                                               [a        , 0              , c / 2],
                                               [a / 2    , a * sqrt(3) / 2, c / 2 + interlayer_dist],
                                               [a * 3 / 2, a * sqrt(3) / 2, c / 2 + interlayer_dist]],
                         coords_are_cartesian=True)
# take supercells
monolayer.make_supercell([Nscale_mono, Nscale_mono, 1])
bilayerAB.make_supercell([Nscale_bilayer, Nscale_bilayer, 1])

# Generate all the samples
monos = [copy_and_perturb(monolayer, perturb_ratio * a) for i in range(Nsamples_mono)]
bilayers = [copy_and_perturb(bilayerAB, perturb_ratio * a) for i in range(Nsamples_bilayer)]
# Take only the coords
monos = np.array([[site.coords for site in sample.sites] for sample in monos])
bilayers = np.array([[site.coords for site in sample.sites] for sample in bilayers])

# Equalize the number of atoms
# min_atoms = min([monolayer.num_sites, bilayerAB.num_sites])
# monos = monos[:, :min_atoms, :]
# bilayers = bilayers[:, :min_atoms, :]

# data = np.concatenate([monos, bilayers])

# Save to a file
np.savez(outfile + ".npz", monos, bilayers)
np.savez(outfile + "_basis.npz", monolayer.lattice.matrix, bilayerAB.lattice.matrix)

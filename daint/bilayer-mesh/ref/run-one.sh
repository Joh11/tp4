#!/bin/bash -l
# The script that will run for each simulation. It is the one that is
# sbatched. It is located inside each simulation directory.

#SBATCH --exclusive
#SBATCH --time=00:30:00
#SBATCH --nodes=1 
#SBATCH --account=s1008
#SBATCH --partition=normal
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node=12 
#SBATCH --constraint=gpu
#SBATCH --job-name=bilayer-mesh
#SBATCH --output=./outputmsg
#SBATCH --error=./errormsg

# Use multicore for now
module load daint-mc

# clean
rm WAVECAR
sed '/end projections/q' wannier90.win

# scf calculation
cp -f INCAR-scf INCAR
cp -f KPOINTS-scf KPOINTS
srun vasp_std
cp OUTCAR OUTCAR-scf
cp OSZICAR OSZICAR-scf
cp EIGENVAL EIGENVAL-scf

rm WAVECAR

# wannier functions
sed -i 's/write_hr = .TRUE./! write_hr = .TRUE./g' wannier90.win 
cp -f INCAR-wannier INCAR
cp -f KPOINTS-wannier KPOINTS
srun vasp_std
cp OUTCAR OUTCAR-wannier
cp OSZICAR OSZICAR-wannier
cp EIGENVAL EIGENVAL-wannier

# run wannier90 v3 to get the hr.dat file
sed -i 's/! write_hr = .TRUE./write_hr = .TRUE./g' wannier90.win 
srun wannier90_v3.0.x wannier90

# remove all the unnecessary UNK files
rm UNK*

#!/bin/bash
# Script to take all the POSCARs, put each one in a different folder,
# put all the necessary files, and sbatch the scripts.

# TODO fill this accordingly
ref_dir=./ref # directory with all the *CAR files and run-one.sh
target_dir=$SCRATCH/bilayer-mesh # usually something in $SCRATCH
poscars=$(ls *.vasp) # usually something like ls POSCAR*
paths="paths.txt"

rm $paths

cur_dir=$(pwd)
echo "Start ..."

for poscar in $poscars
do
    echo "doing $poscar"
    # Copy the ref dir
    cp -r $ref_dir $target_dir/$poscar
    # Copy the poscar
    cp $poscar $target_dir/$poscar/POSCAR
    # Run it
    cd $target_dir/$poscar
    # sbatch run-one.sh
    cd $cur_dir
    echo "$target_dir/$poscar" >> $paths
done

echo "End"

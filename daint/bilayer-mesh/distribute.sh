#!/bin/bash

# paths=$(cat paths.txt)
paths=$(seq 6)

# cmds=$(echo $paths | sed 'N;N;N;N;N;N;N;N;N;s/\n/ /g' | sed 's/^/run-many.sh /')
cmds=$(cat paths.txt | sed 'N;s/\n/ /g' | sed 's/^/sbatch run-many.sh /')
echo $cmds

SAVEIFS=$IFS
IFS=$'\n'

echo "loop: "

for cmd in $cmds; do
    echo "$cmd : "
    eval $cmd
    sleep 1s
done

IFS=$SAVEIFS

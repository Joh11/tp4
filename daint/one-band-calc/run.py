"""A script to run the simulation, and plot the band structure, with
everything automated"""

import os
import numpy as np
import matplotlib.pyplot as plt
import pyprocar as ppc

def clean():
    # Clean everything
    os.system("rm -f INCAR CHG CONTCAR EIGENVAL OSZICAR PCDAT PROCAR.repair WAVECAR vasprun.xml CHGCAR DOSCAR IBZKPT OUTCAR PROCAR REPORT XDATCAR KPOINTS wannier90.mmn wannier90.wout wannier90.amn wannier90.eig wannier90_wsvec.dat wannier90.chk wannier90_hr.dat wannier90_band.* OUTCAR_scf band.agr band.xmgr EFERMI *.dat wannier90.werr")
    # Reset win file
    reset_win()
    update_win(False)

def run():
    clean()
    run_sim()

def reset_win():
    """Remove every line after "end projections" in wannier90.win"""
    with open("wannier90.win") as f:
        lines = f.readlines()
    index = None
    for i, line in enumerate(lines):
        if "end projections" in line:
            index = i
            break
    if not index:
        raise RuntimeError("No end projections found")

    lines = lines[:index + 1] # Drop every line after index

    with open("wannier90.win", "w") as f:
        f.writelines(lines)
    
def update_win(write_hr):
    msg = "write_hr = .TRUE.\n" if write_hr else "! write_hr = .FALSE.\n"

    with open("wannier90.win") as f:
        lines = f.readlines()
        
    lines = [msg if "write_hr" in line else line for line in lines]

    with open("wannier90.win", "w") as f:
        f.writelines(lines)

def run_sim():
    # Step 1 : scf calculation
    print(">>> Starting scf calculation ...")
    # Change KPOINTS
    os.system("cp KPOINTS_scf KPOINTS")
    os.system("cp INCAR_scf INCAR")
    # Run scf
    os.system("vasp_std")
    # Save OUTCAR of scf to get the Fermi energy later
    os.system("cp OUTCAR OUTCAR_scf")

    # Step 2 : band structure calculation
    print(">>> Starting VASP band structure calculation ...")
    # Change KPOINTS
    os.system("cp KPOINTS_band KPOINTS")
    os.system("cp INCAR_band INCAR")
    os.system("rm WAVECAR")

    os.system("vasp_std")

    # Run vp_band.sh
    os.system("./vp2band.sh")

    # Step 3 : VASP + Wannier90
    print(">>> Starting VASP + Wannier90 calculation ...")
    os.system("cp KPOINTS_wannier KPOINTS")
    os.system("cp INCAR_wannier INCAR")
    os.system("rm WAVECAR")
    # Run band computation / Wannier90 1.2
    update_win(False)
    os.system("vasp_std")
    # Run Wannier90 3.0
    update_win(True)
    os.system("wannier90.x wannier90")

def getFermiEnergy():
    with open("EFERMI") as f:
        line = f.readline()
    
    return float(line.split(":")[0])
        
def plot(title=None):
    plt.figure()


    # Load VASP band structure
    EF = getFermiEnergy()
    
    data = np.loadtxt("ek_nxy-gnu.dat")
    ks = data[:, 0]
    Es = data[:, 1] + EF
    plt.scatter(ks, Es, s=0.5, c="blue", label=" VASP output")

    # Load Wannier90 band structure
    data = np.loadtxt("wannier90_band.dat")
    ks = data[:, 0]
    Es = data[:, 1]
    plt.scatter(ks, Es, s=0.5, c="red", label=" Wannier90 output")

    plt.legend(loc='lower right')
    plt.title(title)
    plt.ylabel("E [eV]")
    plt.savefig("bandtest.png")

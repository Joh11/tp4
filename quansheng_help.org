* How to have color in the terminal on Piz Daint
#+BEGIN_SRC shell
  alias ls='ls --color'
#+END_SRC
* How to run jobs on Piz Daint
** Scratch folder
/scratch/snx3000/jfelisaz
Can also use $SCRATCH. 
** Backup
Scratch folder is guaranteed to stay for a week. To make backups, use
#+BEGIN_SRC shell
  rsync -auvP $SCRATCH ~/backup
#+END_SRC
* Programs compiled on Daint
They are located in /project/s1008/software-for-daint. 
* How to compile programs on Daint
** Switch to the intel programming environment
#+BEGIN_SRC shell
  module switch PrgEnv-cray/6.0.5 PrgEnv-intel
#+END_SRC
Use autocompletion to be sure it makes sense with the version. 
** Change the compiler in makefiles
First, use the intel / ifort compatible makefile. Then, change all the
occurences of ifort or mpif90 to ftn. 
* Running job script details
** TODO (make sure it is the right name) ntasks-per-node
- GPU program :: use =1, as there should be only one job for the GPU.
- Multicore program :: use =12, as there are 12 cores on each node
** constraint
*Always* use gpu, as we only have credits for GPU hybrid nodes. 

"""A script to run the simulation, and plot the band structure, with
everything automated"""

import os
import pyprocar as ppc

def clean():
    # Clean everything
    os.system("rm -f INCAR CHG CONTCAR EIGENVAL OSZICAR PCDAT PROCAR.repair WAVECAR vasprun.xml CHGCAR DOSCAR IBZKPT OUTCAR PROCAR REPORT XDATCAR KPOINTS wannier90.mmn wannier90.wout wannier90.amn wannier90.eig wannier90_wsvec.dat wannier90.chk wannier90_hr.dat")
    # Reset win file
    reset_win()
    update_win(False)

def run(plot=False):
    clean()
    run_sim(plot)
    if plot:
        run_pyprocar()
    else:
        run_wannier90()

def run_pyprocar():
    if not os.path.exists("PROCAR.repair"):
        ppc.repair("PROCAR", "PROCAR.repair")
    plt = ppc.bandsplot('PROCAR.repair',outcar='OUTCAR', mode='plain', title='Band structure of graphene', kpointsfile="KPOINTS", exportplt=True)
    plt.savefig("band.png")

def reset_win():
    """Remove every line after "end projections" in wannier90.win"""
    with open("wannier90.win") as f:
        lines = f.readlines()
    index = None
    for i, line in enumerate(lines):
        if "end projections" in line:
            index = i
            break
    if not index:
        raise RuntimeError("No end projections found")

    lines = lines[:index + 1] # Drop every line after index

    with open("wannier90.win", "w") as f:
        f.writelines(lines)
    
def update_win(write_hr):
    msg = "write_hr = .TRUE.\n" if write_hr else "! write_hr = .FALSE.\n"

    with open("wannier90.win") as f:
        lines = f.readlines()
        
    lines = [msg if "write_hr" in line else line for line in lines]

    with open("wannier90.win", "w") as f:
        f.writelines(lines)

def run_wannier90():
    # Run Wannier90 3.0 to generate the hr.dat file
    # Replace the write_hr line with true
    update_win(True)
    
    # Run Wannier90
    os.system("wannier90.x wannier90")
    # os.system("mpirun -np $(nproc) wannier90.x wannier90")

def run_sim(plot):
    # Change KPOINTS
    os.system("cp KPOINTS_scf KPOINTS")
    os.system("cp INCAR_scf INCAR")
    # Run scf
    os.system("vasp_std")
    # Change KPOINTS
    if plot:
        os.system("cp KPOINTS_band KPOINTS")
        os.system("cp INCAR_band INCAR")
    else:
        os.system("cp KPOINTS_wannier KPOINTS")
        os.system("cp INCAR_wannier INCAR")
    os.system("rm WAVECAR")
    # Run band computation / Wannier90
    update_win(False)
    os.system("vasp_std")

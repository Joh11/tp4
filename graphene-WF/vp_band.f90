!> originate from Z.J.Wang
!> modified by Q.S.Wu (wuquansheng@gmail.com) Sep 18, 2018
program main
     implicit none

     ! constant parameters
     integer, parameter :: dp = kind(1.0d0)
     integer, parameter :: mytmp = 1000,myout=547,mylen=548
     real(dp), parameter:: pi=3.141592653589793d0 

     character(len=20)      :: fst
     character(len=20)      :: string
     character(len=5)      :: tmps
     character  backslash*5

     integer :: i, j

     integer :: irow
     integer :: ikpt

     !> Num_bnds is number of bands
     integer :: Num_bnds

     !> num_kpts is the number of kpoints
     integer :: num_kpts

     integer  :: itmp, ityp
     integer  :: kmesh,ISPIN
     real(dp) :: kval, Efermi,a,pa(3),pb(3),pc(3),pra(3),prb(3),prc(3)
     real(dp) :: tmp1, tmp2, tmp3, tmp4
     real(dp) :: ktmp1, ktmp2, ktmp3, ktmp4
     real(dp) :: volumn,t, emin, emax

     real(dp), allocatable :: kdata(:)
     real(dp), allocatable :: kpoint_direct(:, :)
     real(dp), allocatable :: kpoint_cart(:, :)

     character(5), allocatable :: kname(:)
     real(dp), allocatable :: k_len(:)
     real(dp), allocatable :: d_len(:)

     !> energy bands data
     real(dp), allocatable :: Enk(:,:, :)

     ! read fermi level from OUTCAR
     Efermi=0.0d0
     open( mytmp, file="EFERMI", status="old" )
        read(mytmp, *) Efermi 
        read(mytmp, *) ISPIN
     close(mytmp)

     !> Read in lattice vectors from POSCAR in order to get the reciprocal 
     !> lattice vectors
     open( mylen, file="POSCAR",status="old")
     read(mylen,*)
     read(mylen,*)  a
     read(mylen,*)  pa 
     read(mylen,*)  pb 
     read(mylen,*)  pc 
     pa=a*pa
     pb=a*pb
     pc=a*pc
     write(*,*) ' Primitive Cell '
     write(*,'(A8,3(F15.8,2X))') 'a     =',a
     write(*,'(A8,3(F15.8,2X))') 'pa_xyz=',pa
     write(*,'(A8,3(F15.8,2X))') 'pb_xyz=',pb
     write(*,'(A8,3(F15.8,2X))') 'pc_xyz=',pc

     volumn =  pa(1)*(pb(2)*pc(3)-pb(3)*pc(2)) &
             + pa(2)*(pb(3)*pc(1)-pb(1)*pc(3)) &
             + pa(3)*(pb(1)*pc(2)-pb(2)*pc(1))

     write(*,'(A8,3(F15.8,2X))') 'volumn=',volumn
     t=2.0d0*pi/volumn
     
     pra(1)=t*(pb(2)*pc(3)-pb(3)*pc(2))
     pra(2)=t*(pb(3)*pc(1)-pb(1)*pc(3))
     pra(3)=t*(pb(1)*pc(2)-pb(2)*pc(1))
     
     prb(1)=t*(pc(2)*pa(3)-pc(3)*pa(2))
     prb(2)=t*(pc(3)*pa(1)-pc(1)*pa(3))
     prb(3)=t*(pc(1)*pa(2)-pc(2)*pa(1))
     
     prc(1)=t*(pa(2)*pb(3)-pa(3)*pb(2))
     prc(2)=t*(pa(3)*pb(1)-pa(1)*pb(3))
     prc(3)=t*(pa(1)*pb(2)-pa(2)*pb(1))
 
     ! Cartesian Coordinate System for Primitive Riciprocal Cell 
     write(*,*) ' Primitive Riciprocal Cell '
     write(*,'(A9,3(F15.8,2X))') 'pra_xyz=',pra
     write(*,'(A9,3(F15.8,2X))') 'prb_xyz=',prb
     write(*,'(A9,3(F15.8,2X))') 'prc_xyz=',prc
     close(mylen)

     !> Read in the eigenvalues from the EIGENVAL
     open( mytmp, file="EIGENVAL", status="old" )
     open(myout, file='band.xmgr')
     do irow=1,5
         read(mytmp, *)
     enddo

     read(mytmp, *) itmp, num_kpts,Num_bnds

     write(*, '(a, i10)') "Number of k points in total: ", num_kpts
     write(*, '(a, i10)') "Number of bands: ", Num_bnds

     allocate( kdata(num_kpts) )
     allocate( kpoint_direct (3, num_kpts) )
     allocate( kpoint_cart (3, num_kpts) )
     allocate( k_len(num_kpts) )
     allocate( Enk(ISPIN,Num_bnds, num_kpts) )

     kmesh=0
     kval  = 0.0d0
     kdata = 0.0d0
     Enk = 0.0d0
     kpoint_direct= 0.0d0
     kpoint_cart= 0.0d0

     ktmp1= 0d0
     ktmp2= 0d0
     ktmp3= 0d0
     ktmp4= 0d0
     do ikpt=1,num_kpts
        read(mytmp, *)
        read(mytmp, *) tmp1, tmp2, tmp3, tmp4
        kpoint_direct(1, ikpt)= tmp1
        kpoint_direct(2, ikpt)= tmp2
        kpoint_direct(3, ikpt)= tmp3
        kval = kval + tmp4
        if (ikpt .eq. 1) then
          kval=kval-tmp4
          kdata(ikpt) = kval
        else if((abs(tmp1-ktmp1).lt. 0.5d-12).and.(abs(tmp2-ktmp2).lt.0.5d-12).and.(abs(tmp3-ktmp3).lt. 0.5d-12)) then
          kval=kval-tmp4
          kdata(ikpt) = kval
        else if(ikpt.eq. num_kpts) then
          kdata(ikpt) = kval
        else 
          kdata(ikpt) = kval
        endif 
        ktmp1=tmp1
        ktmp2=tmp2
        ktmp3=tmp3
        do irow=1,Num_bnds
            read(mytmp, *) tmps, Enk(:,irow, ikpt)
        enddo
     enddo
     close(mytmp)

     open( mylen, file="KPOINTS" )
     read(mylen, *) 
     read(mylen, *) kmesh
     read(mylen, *) 
     read(mylen, *) 
     ityp=num_kpts/kmesh

     print*, 'Number of k lines=',ityp
     allocate( kname(ityp+1) )
     kname=' '

     open( mytmp, file="EFERMI",  access="append") 
     write(mytmp,"(I1,6X,A4)") ityp, ": NK"
     close(mytmp)

     allocate( d_len(0:ityp) )
     d_len=0.0d0
     do i=1,ityp
      read(mylen, *) pa , kname(i)
      read(mylen, *) pb , kname(i+1)
      pb=pb-pa 
      pc=pb(1)*pra+pb(2)*prb+pb(3)*prc
      d_len(i)=d_len(i-1)+dsqrt(pc(1)**2+pc(2)**2+pc(3)**2)
     enddo
     close(mylen)

     print*, 'k_len:',d_len(:)
     open( mytmp, file="k_len.dat" )
     write(mytmp,"(10F10.5)")  d_len
     close(mytmp)

 
     print*, 'kname : ', kname
     print*, 'kmesh=',kmesh

     open( mytmp, file="EIGENVAL", status="old" )
     do irow=1,5
         read(mytmp, *)
     enddo
     read(mytmp, *) 

     k_len=kdata

     !> from now on, kdata measures the accumulated length
     do ikpt=1,num_kpts
        if(ityp .ne. 0) then
          kval=k_len(ikpt)-k_len((ikpt-1)/kmesh*kmesh+1)
          kdata(ikpt)=(d_len((ikpt-1)/kmesh+1)-d_len((ikpt-1)/kmesh))*kval/(k_len((ikpt-1)/kmesh*kmesh+kmesh) &
              -k_len((ikpt-1)/kmesh*kmesh+1))+d_len((ikpt-1)/kmesh)
        endif
     enddo

     write (myout,'(2f17.10)') kdata(1),-20.0
     write (myout,'(2f17.10)') kdata(1),20.0
     do ikpt=1,num_kpts
        read(mytmp, *)
        read(mytmp, *) tmp1, tmp2, tmp3, tmp4
        if (ikpt .eq. 1) then
        else if((abs(tmp1-ktmp1).lt. 0.5d-12).and.(abs(tmp2-ktmp2).lt.0.5d-12).and.(abs(tmp3-ktmp3).lt. 0.5d-12)) then
          write(myout, *) "&"
          write(myout, *) "&"
          write (myout,'(2f17.10)') kdata(ikpt),-20.0
          write (myout,'(2f17.10)') kdata(ikpt),20.0
        else if(ikpt.eq. num_kpts) then
          write(myout, *) "&"
          write(myout, *) "&"
          write (myout,'(2f17.10)') kdata(ikpt),-20.0
          write (myout,'(2f17.10)') kdata(ikpt),20.0
        else 
        endif 
        ktmp1=tmp1
        ktmp2=tmp2
        ktmp3=tmp3
        do irow=1,Num_bnds
            read(mytmp, *) tmps, Enk(:,irow, ikpt)
        enddo
     enddo
    close(mytmp)


    ! export energy bands data to bands.xmgr
     do irow=1,Num_bnds
     write(myout, *) "&"
         do ikpt=1,num_kpts
             write(myout, '(2f17.10)') kdata(ikpt), Enk(1,irow, ikpt)-Efermi
         enddo
     enddo
     close(myout)

     !> band.agr
     open(myout, file='band.agr')
     write(myout,*) "#    k       ene"
     write(myout,*) '@ page size 595, 842'      
     write(myout,*) "# Tune the size of the plot, left bottom"
     write(myout,*) '@ view 0.120000, 0.500000, 0.650000, 0.900000'
     write(myout,*) '@ default linewidth 2.0'
     write(myout,*) '@ xaxis  label char size 1.5'
     write(myout,*) '@ xaxis  ticklabel char size 1.25'
     write(myout,*) '@ yaxis  label char size 1.5'
     write(myout,*) '@ yaxis  ticklabel char size 1.25'
     write(myout,*) '@ xaxis  tick major grid on'
     write(myout,*) '@ xaxis  tick spec type both'
     write(myout,*) '@ xaxis  tick spec', ityp+1

     !> set vertical lines to separate different regions
     do j=1,ityp+1
        if(j.le.ityp) then
         write(*,*) 'line',j,' is: ',kname(j),' to ', &
                    kname(j+1)
         write(*,*) 'xmax=',d_len(j-1)
        endif 
!...xmgrace: write xaxis tick labels to bands.agr;
!            translate Greek letters into xmgrace syntax
        backslash='" xG"'
        backslash(2:2)=achar(92)
        write(myout,224) j-1, d_len(j-1)
224     format('@ xaxis  tick major ',i3,',',f8.5)
        if((kname(j)(1:5).eq.'GAMMA').or.(kname(j)(1:5).eq.'Gamma').or.(kname(j)(1:3).eq.'Gam')) then
           backslash(4:4)='G'
           write(myout,*) '@ xaxis  ticklabel ',j-1,',',backslash
        else
           write(myout,*) '@ xaxis  ticklabel ',j-1,',"',kname(j),'"'
        endif
      enddo

      emin=  minval(Enk)-Efermi
      emax=  maxval(Enk)-Efermi
      emin=  -1d0
      emax=  1d0

      !...xmgrace: write xmin, ymin, xmax, ymax for the plot to case.bands.agr
      write(myout,'(A)') '@ with g0'
      write(myout,701) emin,d_len(ityp), emax
701     format('@ world 0, ',f12.5,',',f12.5,',',f12.5)
      write(myout,*) "@ autoticks"
      !...xmgrace: label yaxis (written to bands.agr)
      write(myout,*) '@ yaxis  label "Energy(eV)"'
      
      ! ...xmgrace: insert line at Fermi energy (written to case.bands.agr)
      write(myout,*) '@ with line'
      write(myout,*) '@ line on'
      write(myout,*) '@ line loctype world'
      write(myout,222) 0.d0, 0.d0, d_len(ityp), 0.d0
222     format('@ line ',f8.5,',',f8.5,',', f8.5,',', f8.5)      
      write(myout,*) '@ line linestyle 3'
      write(myout,*) '@ line def'    
      ! ...xmgrace: label Fermi energy#
      write(myout,*) '@ with string'
      write(myout,*) '@ string on'
      write(myout,*) '@ string loctype world'
      write(myout,223) d_len(ityp)+0.02
223     format('@ string ', f8.5,', -0.1')      
      write(myout,*) '@ string char size 1.500000'
      write(myout,*) '@ string def "E'//achar(92)//'sF"'

      !...xmgrace: write title to case.bands.agr
      write(myout,124) 'I am the title.'
 124  format(' @ title "',a,'"')


     do irow=1, Num_bnds
         write(myout, *) "@ autoscale onread none"
          write(fst,*) irow ; fst=adjustl(fst)
          string="@ target g0.s"//trim(fst)
         write(myout, *) string
         write(myout, *) "@ type xy"
         write(myout, *) 
         write(myout, "(A13,I3)")  "# bandindex:",irow
         do ikpt=1,num_kpts
             write(myout, '(2f17.10)') kdata(ikpt), Enk(1,irow, ikpt)-Efermi
         enddo
         write(myout, *) "&"
     enddo
     close(myout)

     !> xmgr format
     if (ISPIN == 2)  then
        open(myout, file='band_dn.xmgr')
        do irow=1,Num_bnds
        write(myout, *) "&"
            do ikpt=1,num_kpts
                write(myout, '(2f17.10)') kdata(ikpt), Enk(2,irow, ikpt)-Efermi
            enddo
        enddo
        close(myout)
        
        open(myout, file='band_dn.agr')
        do irow=1,Num_bnds
            write(myout, *) "@ autoscale onread none"
             write(fst,*) irow+Num_bnds ; fst=adjustl(fst)
             string="@ target g0.s"//trim(fst)
            write(myout, *) string
            write(myout, *) "@ type xy"
            write(myout, *) 
            write(myout, "(A13,I3)")  "# bandindex:",irow
            do ikpt=1,num_kpts
                write(myout, '(2f17.10)') kdata(ikpt), Enk(2,irow, ikpt)-Efermi
            enddo
            write(myout, *) "&"
        enddo
        close(myout)
     endif

     open(myout, file='ek_nxy-gnu.dat')
     do irow=1, Num_bnds
         do ikpt=1,num_kpts
             write(myout, '(10000f17.10)') kdata(ikpt), Enk(1,irow, ikpt)-Efermi
         enddo
         write(myout, *)' '
     enddo
     close(myout)

     open(myout, file='ek_nxy.dat')
         do ikpt=1,num_kpts
             write(myout, '(10000f17.10)') kdata(ikpt), Enk(:,:, ikpt)-Efermi
         enddo
     close(myout)


     stop
end program main

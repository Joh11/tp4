import numpy as np
import pymatgen as mg
from math import sqrt, exp
from pymatgen.io.vasp import Poscar

Catom = mg.Element("C")

# def perturb_gaussian(structure, pos, spread, amplitude):
#     """Adds a gaussian noise centered at the given position, with the
#     given spread and amplitude. Everything is deterministic here."""
#     def gaussian(coords):
#         return amplitude * exp(- ((coords - pos) ** 2).sum() / 2. / spread / spread)
    
#     for i in range(len(structure._sites)):
#         pos_site = structure._sites[i].coords[:2] # Take only x and y
#         translation_vec = [0, 0, gaussian(pos_site)]
#         print("trans vec {}".format(translation_vec))
#         translation_vec = structure.lattice.get_fractional_coords(translation_vec)
#         print("trans vec (fractional) {}".format(translation_vec))
#         print("pos site {}".format(structure._sites[i].coords))
#         structure.translate_sites([i], translation_vec)
#         print("pos site after translation{}".format(structure._sites[i].coords))
    

# everything is in Angstroms
# a = 2.46 # source : wikipedia Graphene page
a = 1.42
c = 20 # just a big number
interlayer_dist = 3.35 # according to https://arxiv.org/pdf/1311.2164.pdf


# build a hexagonal lattice
lattice = mg.Lattice([  [a * 3 / 2, a * sqrt(3) / 2, 0]
                      , [0        , a * sqrt(3)    , 0]
                      , [0        , 0              , c]])

monolayer = mg.Structure(lattice, 2 * [Catom], [[0, 0, 1/2], [2/3, 2/3, 1/2]], coords_are_cartesian=False)
bilayerAB = mg.Structure(lattice, 4 * [Catom], [[0, 0, 1/2 - interlayer_dist / (2 * c)]
                                              , [2/3, 2/3, 1/2 - interlayer_dist / (2 * c)]
                                              , [1/3, 1/3, 1/2 + interlayer_dist / (2 * c)]
                                              , [2/3, 2/3, 1/2 + interlayer_dist / (2 * c)]]
                         , coords_are_cartesian=False)

bilayerAA = mg.Structure(lattice, 4 * [Catom], [[0, 0, 1/2 - interlayer_dist / (2 * c)]
                                              , [2/3, 2/3, 1/2 - interlayer_dist / (2 * c)]
                                              , [0, 0, 1/2 + interlayer_dist / (2 * c)]
                                              , [2/3, 2/3, 1/2 + interlayer_dist / (2 * c)]]
                         , coords_are_cartesian=False)

# monolayer.make_supercell([5, 5, 1])
# perturb_gaussian(monolayer, monolayer.lattice.get_cartesian_coords([1/2, 1/2, 0])[:2], 2.0, 2.)
# Poscar(monolayer).write_file("POSCAR")
Poscar(bilayerAA).write_file("POSCAR")

def bilayer_struct(vec):
    """Returns a bilayer graphene structure, where the top layer is shifted by vec. 
    AB stacking : vec = [0, 0]
    AA stacking : vec = [1/3, 1/3]"""
    vec = np.array(vec)
    if np.any(vec < 0) or np.any(vec >= 1):
        raise RuntimeError("Invalid vector")

    return mg.Structure(lattice, 4 * [Catom], [[0, 0, 1/2 - interlayer_dist / (2 * c)]
                                              , [2/3, 2/3, 1/2 - interlayer_dist / (2 * c)]
                                              , [1/3 + vec[0], 1/3 + vec[1], 1/2 + interlayer_dist / (2 * c)]
                                              , [2/3 + vec[0], 2/3 + vec[1], 1/2 + interlayer_dist / (2 * c)]]
                        , coords_are_cartesian=False)

def generate_poscars(N=10):
    ns = np.arange(N) / N

    for x in ns:
        for y in ns:
            Poscar(bilayer_struct([x, y])).write_file("POSCAR_{}_{}".format(x, y))

"""A script to read the wannier90.wout file"""

def get_spreads(path="wannier90.wout"):
    with open(path) as f:
        lines = f.readlines()
    lines = [line if "spread" is in line for line in lines]
    print(lines)

import numpy as np
from os import system
from poscar import bilayer_struct

N = 10 # Number of samples per direction (N_tot = N ** 2)
ns = range(N)
original_dir = "~/tp4/ref"

for x in ns:
    for y in ns:
        system("cp -r {} bilayer_{}_{}".format(original_dir, x, y))
        Poscar(bilayer_struct([x/N, y/N])).write_file("bilayer_{}_{}/POSCAR".format(x, y))
        system("cd bilayer_{}_{}; sbatch run.sh; cd ..".format(x, y))

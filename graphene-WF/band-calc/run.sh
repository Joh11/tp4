#!/bin/bash -l
#
#SBATCH --exclusive
#SBATCH --time=0:15:00
#SBATCH --nodes=1
##SBATCH --partition=normal
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=28
#SBATCH --job-name=band-calc
#SBATCH --output=./band-calc.out
#SBATCH --error=./band-calc.err


clean()
{
    rm -f INCAR CHG CONTCAR EIGENVAL OSZICAR PCDAT PROCAR.repair WAVECAR vasprun.xml CHGCAR DOSCAR IBZKPT OUTCAR PROCAR REPORT XDATCAR KPOINTS wannier90.mmn wannier90.wout wannier90.amn wannier90.eig wannier90_wsvec.dat wannier90.chk wannier90_hr.dat wannier90_band.* OUTCAR_scf band.agr band.xmgr EFERMI *.dat wannier90.werr band-calc.err band-calc.out
    python -c "import run; run.reset_win()"
}

# First clean
clean

module purge
module load intel
module load intel-mpi
# Then simulate

echo ">>> Starting scf calculation ..."
cp KPOINTS_scf KPOINTS
cp INCAR_scf INCAR
srun vasp_std

cp OUTCAR OUTCAR_scf

echo ">>> Starting VASP band structure calculation ..."
cp KPOINTS_band KPOINTS
cp INCAR_band INCAR
rm WAVECAR

srun vasp_std

# Run vp_band.sh to extract useful infos
./vp2band.sh

echo ">>> Starting VASP + Wannier90 calculation ..."
cp KPOINTS_wannier KPOINTS
cp INCAR_wannier INCAR
rm WAVECAR

python -c "import run; run.update_win(False)"
srun vasp_std
python -c "import run; run.update_win(True)"
srun wannier90.x wannier90

python -c "import run; run.plot()"

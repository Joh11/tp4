from pyprocar import *

# plot the whole band structure to check which bands are close to the Fermi level
bandsplot('PROCAR',outcar='OUTCAR', mode='plain', title='Band structure of graphene')

# We can see 8 bands.  Two of theme are close to the Fermi level. We
# can see one Dirac cone.

# Now projecting onto the s orbitals : 
bandsplot('PROCAR',outcar='OUTCAR', mode='parametric', cmap='hot_r', orbitals=[0])
# They do not contribute to the 2 bands near the FM. They are mostly
# responsible for the lower energy bands, and a bit for the high
# energy ones.

# Now onto the py px orbitals:
bandsplot('PROCAR',outcar='OUTCAR', mode='parametric', cmap='hot_r', orbitals=[1, 3])
# Again, they do not contribute to the 2 interesting bands.

# Now onto the pz:
bandsplot('PROCAR',outcar='OUTCAR', mode='parametric', cmap='hot_r', orbitals=[2])
# It indeed contribute almost exclusively to the bands of
# interest. Thus we will restrict our analysis to the pxorbitals.

# Now we want to find the disentanglement window dis_win. We can
# clearly see that the upper band goes to something around 6.5, and
# the lower band to something like 5.5. Thus we get dis_win = [-5.5,
# 6.5]

# Now we want the frozen window. It is found to be froz_win = [-2.5,
# 3.5]

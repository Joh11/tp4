"""Produce descriptors similar to the ones used in DeePMD-kit"""

import numpy as np

# TODO implement the "full information cutoff radius"
# But try to see if it is computationnally viable without, and if it
# does improve the error rate

epsilon = 1e-8
N = 500
Nc = 100
Rc = 10

def normalize(v):
    return v / np.linalg.norm(v)

def sort_indices(types, distances):
    """Returns an array of indices, sorting elements according to the
    following rules: 
    - First the elements are put in descending order of their atomic number
    - Then they are sorted with ascending order in their inverse distances
    """
    types = -types # To reverse the order
    inv_dists = 1 / distances

    arr = np.array(list(zip(types, inv_dists)), dtype=[("type", np.float32), ("inv_dist", np.float32)])

    return np.argsort(arr, order=["type", "inv_dist"])

def make_descriptor(coords, types):
    """Take an Nx3 coords array, and a N types [atomic numbers here] array
    and produce a (N, Nc, 4) descriptor and a (N, Nc) array of types
    for each neighborhood
    """
    print(coords)
    print(types)

    # Fix the number of atoms
    if types.size > N:
        coords = coords[:N, :]
        types = types[:N]

    Natoms = types.size
        
    D = np.zeros((N, Nc, 4))
    types_corrected = np.zeros((N, Nc))

    for i in range(Natoms):
        R = coords[i, :]
        neighbors_mask = (np.linalg.norm(coords - R, axis=1) <= Rc) * (np.arange(Natoms) != i)

        coords_n = coords[neighbors_mask, :]
        coords_n -= R

        types_n = types[neighbors_mask]
        norms_n = np.linalg.norm(coords_n, axis=1)

        Nneighbors = types_n.size
        # print("Atom {} has {} nearest neighbors".format(i, Nneighbors))

        indices_n = sort_indices(types_n, norms_n)
        coords_n = coords_n[indices_n, :]
        types_n = types_n[indices_n]
        norms_n = norms_n[indices_n]

        if Nneighbors > Nc: # Remove the excess neighbors
            # print("Removing excess neighbors")
            coords_n = coords_n[Nneighbors - Nc:, :] 
            norms_n = norms_n[Nneighbors - Nc:]
            types_n = types_n[Nneighbors - Nc:]
        
        if Nneighbors >= 2:
            # Now that they are sorted, change the basis
            # Be careful that the two vectors are not colinear
            
            e1 = normalize(coords_n[0, :])
            j = 1
            while np.linalg.norm(np.cross(e1, coords_n[j, :])) < epsilon:
                print("e1 = {}, e2? = {}".format(e1, coords_n[j, :]))
                j += 1
                print("shift")
            print("now : {}".format(np.cross(e1, coords_n[j, :])))
            e2 = normalize(coords_n[j, :] - np.dot(coords_n[j, :], e1) * e1)
            e3 = np.cross(e1, e2)

            mat = np.column_stack([e1, e2, e3])
            
            coords_n = coords_n @ mat
        
        # Fill the descriptor tensor
        D[i, :Nneighbors, 0] = 1 / norms_n
        D[i, :Nneighbors, 1:4] = D[i, :Nneighbors, 0].reshape((-1, 1)) * coords_n

        # And the types after the sorting process
        types_corrected[i, :Nneighbors] = types_n

        if np.sum(np.isnan(D)) > 0:
            raise Exception("nan in the {} atom".format(i))

    return D, types_corrected

coords_file = np.load("data/coords.npz")
types_file = np.load("data/type.npz")
energy_file = np.load("data/energy.npz")

file_names = coords_file.files

Ds = []
ts = []
es = []
for f in file_names:
    descriptor, types = make_descriptor(coords_file[f], types_file[f])
    energy = energy_file[f]

    print("Descriptor for {} : {}".format(f, descriptor))
    
    Ds.append(descriptor)
    ts.append(types)
    es.append(energy)

D = np.stack(Ds, axis=0)
t = np.stack(ts, axis=0)
e = np.stack(es, axis=0)

np.save("data/desc/descriptor.npy", D)
np.save("data/desc/type.npy", t)
np.save("data/desc/energy.npy", e)

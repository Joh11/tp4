* Roadmap
** Setup everything on Piz Daint
*** Run tensorflow
** Now that I have the june5 dataset
*** See why tf I get zeros and -0.5s in my nearest neighbor coupling
*** Try to map the hopping energy with the shifting vector (see if it makes sense ?)
*** Tinker with t-SNE parameters to see if I get something less ugly
* Gists
** How to run Wannier90
1. SCF calculation
2. Take the CHGCAR from it
3. Set LORBIT=11, ICHARG=11, LWANNIER90=.TRUE. in INCAR
4. Take the same KPOINTS but with at least 2 in the z direction (bug with Wannier90 ?)
5. Prefill the .win file with num_wann=Natoms_per_unit_cell * Nprojector = 2, and begin projector
** Unit cell definition
*** Take the 2nd carbon atom in the unit cell, i.e. (2/3, 2/3, 0) in direct coords
*** Shift by a_3 / 2 the atoms to get them in the center of the unit cell in z
** Try with bilayer, perturbed monolayer, and trilayer
*** Band structure should not change that much with small perturbations
** Use the folder in graphene-WF/band-calc/ to compute the band structure using both VASP and Wannier90, automatically
*** Modify the POSCAR to load the right material
*** Set NBANDS in INCAR's and .win file to make sure the bands are fully displayed
*** from run import *; run(); plot()
*** The result is located in bandtest.png
** Without the dis / froz window there is a greater difference between VASP and Wannier90 band structure
** Handy command to run sbatch, make sure it is running, and show the output after
sbatch run.sh ; watch -n1 Squeue ; sleep 1s; tail -f band-calc.out
** When using N nodes, NBANDS must be a multiple of N * 28
** When using Descriptor.describe(i, j, R) indices start at zero
** When looking at the POSCAR data in VESTA, it starts at one
** How to connect directly to Piz Daint cluster (setup ssh proxy)
Put in the *.ssh/config* file : 
#+BEGIN_EXAMPLE
  Host=ela.cscs.ch
  User=jfelisaz

  Host=daint.cscs.ch
  User=jfelisaz
  ProxyCommand=ssh ela.cscs.ch nc daint.cscs.ch 22
#+END_EXAMPLE
* Ideas
** Maybe learn the shape of Wannier functions instead of the hopping parameters
** Using an histogram based descriptor
* Things I tried
** Descriptor
*** Bond invariance
**** Take the one closest to center of mass
**** Smallest sum of distance
**** Smallest sum of distance**2
**** Smallest sum of sqrt distance
**** Biggest number of atom on a side
**** Distance to the normal bonding plane
**** Lets assume it is good enough for now
* Questions for QuanSheng
* Quansheng's answers
** Append the length or put the two atoms ? 
*** Solve it using his version of the descriptor
** The variation throughout all the simulations does not seem greater than the error we have with Wannier90; how to reduce the error ?
*** It is fine we do not really care about this part
*** Though we can increase the frozen window to cover the plateau around the M point (?)
*** Making sure the bottom of the higher energy bands is not taken though
** Disentanglement not reached ? 
*** It is fine because the disentanglement step will mostly affect the part far from the K point, with many other bands
*** And if it is too long we may have issue with the gauge invariance
** Show him the plots in processing.py
*** Seems fine ! Keep only up until 6 Å
** So keeping only less than 5 Å of distance is ok ?
*** cf above
** How to reduce nrpts in Wannier90 ?
*** Don't care it's fine
** Is the order of WFs the same as the order of the atoms in the POSCAR ?
*** Yes always
** If a site is defined in the POSCAR outside of the unit cell does it break everything ?
*** Put a modulo 1 to be safe
*** And a translate_home_cell to be sure
** num_bands : same as in the VASP calculation, or the number of bands we focus on in the end ? i.e. 25 or 2 ?
*** Use 25
** Which parameters do I have to set ? 
*** num_wann, num_bands, begin projections, write_hr, num_iter=0, dis_num_iter=100
** Why I have strange error when I run wannier90 in parallel
*** Use srun in a script like before
** Do the dis / froz window for both runs of Wannier90 ?
*** Yes, does not matter
** How to plot the band structure from hr.dat ?
*** bands_plot = .TRUE. 
** How to know which parameters should I put in SBATCH scripts
** Longer time (contract changed) ?
*** No it was the date until we can access the files, not run jobs
* Todo
** DONE Plot the (absolute value of the) hopping parameter as a function of distance
** DONE use a multiple of 3 for KPOINTS grid to make sure the special K point land on it
** DONE plot WF to make sure they are localized
*** 332 kpts : they are localized even though the spread is huge for some WFs
*** 442 kpts : localized, no huge spreads
** DONE Check if translating sites automatically loop them back on the other side
** add all the INCAR params
** read ISMEAR man page
** Fix the gauge invariance problem
*** As it can be seen on the 442kpts 551 unperturbed supercell, there is already a problem of inverted lobes
* Notes
using hard POSCAR : the top of pz band is the 18th band

15min, LREAL = .FALSE., 4 cores : not enough
try with 3h both LREAL cases. 

EDIFF 1e-6 / 1e-7
try with soft one
ISMEAR= -5
sigma ?
encut 1.5 enmax

11 bands for unit cell
LWAVE=F


module switch prgenv prgrenv intel

https://user.cscs.ch/computing/compilation/
Enabling GPU support in the programming environment

The GPU programming environment is not readily available when you log
in: if you want to compile CUDA code, you should load the module
craype-accel-nvidia60. This module will load in turn the cudatoolkit
module, making available the nvcc compiler, the CUDA runtime
environment and all the CUDA programming tools (profilers, debuggers
etc.). In addtion, it will also set the target accelerator
architecture to the Tesla P100 GPU (CRAY_ACCEL_TARGET environment
variable) and allow you to compile OpenACC code with the Cray
compiler. If not loaded, Cray compiler will ignore the OpenACC
directives, because a target accelerator could not be found. For
compiling OpenACC code with the PGI compiler, the CRAY_ACCEL_TARGET
variable is not necessary, but the cudatoolkit module must be loaded.

all: std gam ncl gpu gpu_ncl

sbatch account s238h
constraint

max amplitude 0.15 A (2 * for bilayer)
interlayer dist 3.35 A
AB or AA ? all stacking
how does the band structure looks like for bilayer (AA / AB) ?
spread ? 20 to 30 A random

neighbors
negative 38
negative 19
positive 18
** What we want in the end
Not a neural network capable of computing the hopping parameters for
any system, but rather one capable of doing so for systems that are
perturbed bilayers, of every kind from AB to AA stacking, including SP
stacking along the way. The perturbation corresponds to Gaussian
deformations along the z direction.

* Links given by Quansheng
** Neural networks
*** [[https://www.youtube.com/watch?v=wvsE8jm1GzE][A.I. Experiments : Visualizing High-Dimensional Space]]
*** Permutation issues
- [[https://ai.stackexchange.com/questions/4655/permutation-invariant-neural-networks][Permutation invariant neural networks]]
- [[https://github.com/off99555/superkeras/blob/master/permutational_layer.py][Permutational layers in superkeras]]
** ML for materials
- [[https://arxiv.org/pdf/1909.02041.pdf][Regression-clustering with Molecular-Orbital-Based ML]]

* For the report
** What to put in it ?

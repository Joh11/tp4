"""A script to generate a descriptor from a POSCAR file or similar,
using the method outlined in the DeePMD paper"""

import numpy as np
import pymatgen as mg

def generate_descriptor(structure):
    

def descriptor_from_file(path):
    return generate_descriptor(mg.Structure.from_file(path), max_neighbors=[("C", 10)])

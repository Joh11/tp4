"""A script to generate the descriptor of a bond, using a method
similar to the one in the DeePMD paper"""

import numpy as np
import pymatgen as mg

def unzip(l):
    return [list(t) for t in zip(*l)]

def normalize(v):
    return v / np.linalg.norm(v)

def sort_filter_coords(coords, species, max_neighbors):
    # First by following max_neighbors ordering for species
    # Then by increasing distance

    coords_list = []
    
    for specie, max_n in max_neighbors:
        # Take only the right atoms
        scoords = np.row_stack([c for c, s in zip(coords, species) if str(s) == specie])
        idcs = np.argsort(np.linalg.norm(scoords, axis=1))
        # Sort them and only take the max_neighbors first ones
        scoords = scoords[idcs][:min(max_n, scoords.shape[0])]
        
        final_vec = np.zeros((max_n, 3))
        final_vec[:] = np.nan
        end=min(max_n, scoords.shape[0])
        final_vec[:end] = scoords[:end]
        coords_list.append(final_vec)

    return np.row_stack(coords_list)

def make_basis(coords, center, site2_coords):
    # e1 in the direction of the second atom
    e1 = normalize(site2_coords - center)
    print("e1={}".format(e1))

    # e2 in the direction of the furthest atom
    e2 = np.array([0, 0, 0])
    coordsp = np.copy(coords)
    while(np.linalg.norm(e2) == 0):
        if len(coordsp) == 0:
            raise RuntimeError("All the neighbors are colinear : {}".format(coords))
        idx = np.nanargmax(np.linalg.norm(coordsp, axis=1))
        e2 = coordsp[idx, :]
        e2 = e2 - e2.dot(e1) * e1
        print("e2={}".format(e2))
        coordsp = np.delete(coordsp, idx, axis=0)

    e2 = normalize(e2)
    e3 = np.cross(e1, e2)
    return np.column_stack([e1, e2, e3])

def extract_radial(coord):
    if np.isnan(coord).any():
        return np.array([0, 0, 0, 0])
    R = np.linalg.norm(coord)
    return np.append(coord, [1]) / R


class DescriptorGenerator:
    def __init__(self, path, max_neighbors, bond_radius_cutoff=1.8, neighbor_radius_cutoff=2):
        self.structure = mg.Structure.from_file(path)
        
        self.max_neighbors = max_neighbors
        self.bond_radius_cutoff = bond_radius_cutoff
        self.neighbor_radius_cutoff = neighbor_radius_cutoff

    def descriptor(self, i, j, R):
        "Returns the descriptor for the bond between the atom on site (i, 0) to the one on (j, R)"
        site1 = self.structure[i]
        site2 = self.structure[j]

        center = (site1.coords + site2.coords) / 2
        

    def descriptor_from_bond(self, structure, site, site2):
        # max_neighbors is like [("C", 5), ("N", 5)]
        center = (site.coords + site2.coords) / 2
        # Grab the nearest sites (except the two atoms)
        sites, coords = unzip([(x[0], x[-1]) for x in structure.get_sites_in_sphere(center,
                                                                                    self.neighbor_radius_cutoff,
                                                                                    include_image=True)
                               if x[0] != site and x[0] != site2])
        
        # Translation invariance
        coords = np.row_stack(coords) - center
        species = [s.specie for s in sites]
        # print("species={} and coords={}".format(species, coords))
        coords = sort_filter_coords(coords, species, self.max_neighbors)
        print("coords after sort_filter = {}".format(coords))
        # Rotation invariance
        new_basis = make_basis(coords, center, site2.coords)
        
        print("det = {}".format(np.linalg.det(new_basis)))
        coords = np.row_stack([np.linalg.inv(new_basis) @ x for x in coords])
        print("coords after rotation = {}".format(coords))
        
        # [x, y, z] -> [x/R, y/R, z/R, 1/R]
        coords = np.row_stack([extract_radial(x) for x in coords])
        
        # turn it into a vector
        return coords.reshape((-1,))

        
    def all_descriptors(self):
        """If we enter a material with N atoms basis, we'll get back a Nbonds * (1 + 20 * 4) vector"""
        descriptors = []
        bonds = []    
        for i in range(len(self.structure)):
            print("i={}".format(i))
            site = self.structure[i]
            specie = site.specie
            sites, dists, js, Rs = unzip(self.structure.get_neighbors(site, self.bond_radius_cutoff))
            
            for site2, j, R in zip(sites, js, Rs):
                print("j={}".format(j))
                # If already done, skip it
                if j <= i:
                    print("skipped")
                    continue
                
                # Maybe flip according to the largest Z first
                specie2 = site2.specie
                if specie.Z > specie2.Z:
                    d = self.descriptor_from_bond(self.structure, site2, site)
                    bond = (site2, site)
                else:
                    d = self.descriptor_from_bond(self.structure, site2, site)
                    bond = (site, site2)
                    
                    descriptors.append(d)
                    bonds.append(bond)
                    
        return descriptors, bonds

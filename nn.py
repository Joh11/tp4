import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

import matplotlib.pyplot as plt

def plot_history(histories, key='mse'):
    plt.figure(figsize=(16,10))
    
    for name, history in histories:
        val = plt.plot(history.epoch, history.history['val_'+key],
                       '--', label=name.title()+' Val')
        plt.plot(history.epoch, history.history[key], color=val[0].get_color(),
                 label=name.title()+' Train')
        
    plt.xlabel('Epochs')
    plt.ylabel(key.replace('_',' ').title())
    plt.legend()

    plt.xlim([0,max(history.epoch)])


# TODO for now just don't care about the atomic number of the atoms

Ds = np.load("data/desc/descriptor.npy")
energy = np.load("data/desc/energy.npy")

# Load training and test datasets
Ds = Ds.reshape((Ds.shape[0], Ds.shape[1], -1))

# TODO find where are the NaN from
mask = np.product(np.product(np.isfinite(Ds), axis=1), axis=1).astype(bool)
Ds = Ds[mask, :, :]
energy = energy[mask]

randomize = np.arange(Ds.shape[0])
np.random.shuffle(randomize)
Ds = Ds[randomize]
energy = energy[randomize]

Ds_train = Ds[:40]
energy_train = energy[:40]

Ds_test = Ds[40:]
energy_test = energy[40:]

# dataset = tf.data.Dataset.from_tensor_slices((Ds, energy))
# dataset = dataset.shuffle(100)
# dataset = dataset.batch(2)

# Model
class MyLayer(layers.Layer):
    def __init__(self):
        super(MyLayer, self).__init__()

    def build(self, input_shape):
        # Layers to apply only on each rank 1 subtensor
        self.sublayers = [layers.Dense(240, activation="tanh", input_shape=input_shape),
                          layers.Dense(120, activation="tanh"),
                          layers.Dense(60, activation="tanh"),
                          layers.Dense(30, activation="tanh"),
                          layers.Dense(1)]
        # self.sublayers = [layers.Dense(1, input_shape=(400,))]
        super(MyLayer, self).build(input_shape)

    def apply_sublayers(self, input):
        x = input
        # print(x.shape)
        for l in self.sublayers:
            x = l(x)
        # print("x done")
        return x

    def call(self, inputs):
        # print("bojnour {}".format(inputs.shape))
        inputs = tf.transpose(inputs, perm=[1, 0, 2])
        # print("auuirzued {}".format(inputs.shape))
        outputs = tf.map_fn(self.apply_sublayers, inputs)
        return tf.transpose(outputs, perm=[1, 0, 2])

    def compute_output_shape(self, input_shape):
        return (input_shape[0], 1)

inputs = keras.Input(shape=Ds.shape[1:])
atomic_energies = MyLayer()(inputs)
total_energy = keras.backend.sum(atomic_energies, axis=1)

model = keras.Model(inputs=inputs, outputs=total_energy)
model.compile(optimizer=keras.optimizers.Adam(),
              loss=keras.losses.MeanSquaredError(),
              metrics=["mse", "mape"])

train_history = model.fit(Ds_train, energy_train, epochs=100, batch_size=10, validation_data=(Ds_test, energy_test))
# model.load_weights("weights_8_percent.model")

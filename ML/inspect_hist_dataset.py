import numpy as np
import h5py as h5
import matplotlib.pyplot as plt

# Import data
with h5.File('dataset_hist_nn.hdf5', 'r') as f:
    train_examples, train_labels = f['data'][:], f['labels'][:]

    
def pca(x, p=None, return_u=False):
    C = x.T @ x / (len(x) - 1) # Covariance matrix
    eigenvals, eigenvecs = np.linalg.eig(C)

    if p is None:
        p = len(eigenvecs)

    u = eigenvecs[:, :p].real

    if return_u:
        return x @ u, u
    return x @ u

vecs = pca(train_examples)

# Plot the first and second PCA against the labels (using a cmap)
# plt.figure()
# plt.scatter(vecs[:, 0], vecs[:, 1], c=train_labels)

# plt.title('PCA 0 vs PCA 1')
# plt.xlabel('PCA 0')
# plt.ylabel('PCA 1')
# plt.colorbar()

# Plot the norm of each descriptor against the labels
plt.figure()
plt.scatter(np.linalg.norm(train_examples, axis=1), train_labels)

plt.title('Norm against label')
plt.xlabel('norm')
plt.ylabel('label')

plt.show()

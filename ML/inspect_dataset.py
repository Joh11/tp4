#!/usr/bin/env python3

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import time

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

# Import data
# -----------

f = h5.File('dataset_june5.hdf5', 'r')
train_examples, train_labels = f['data'][:], f['labels'][:].reshape((-1,))

# remove the strange ones TODO UNDERSTAND
mask = train_labels < -1.
train_examples, train_labels = train_examples[mask, :], train_labels[mask]

nsamples = train_examples.shape[0]
nfeatures = train_examples.shape[1]

# PCA
# ---

pca = PCA()
pca_result = pca.fit_transform(train_examples)

# Plot the relative variance of each component
plt.figure()
plt.scatter(np.arange(nfeatures), 100 * pca.explained_variance_ratio_)

plt.xlabel("Component index")
plt.ylabel("Explained variance ratio [%]")

# Plot the labels against the 1st and 2nd component of the PCA
plt.figure()
plt.scatter(pca_result[:, 8], pca_result[:, 9], c=train_labels)

plt.xlabel("PCA 8")
plt.ylabel("PCA 9")
plt.colorbar()

# Plot the labels against PCA 0, 1, and 2
# ax = plt.figure().add_subplot(111, projection='3d')
# p = ax.scatter(pca_result[:, 0], pca_result[:, 1], pca_result[:, 2], c=train_labels)

# plt.gcf().colorbar(p)
# ax.set_xlabel("PCA 0")
# ax.set_ylabel("PCA 1")
# ax.set_zlabel("PCA 2")

# t-SNE
# -----

# perm = np.random.permutation(nsamples)
# nfew = 10000

# few_examples = train_examples[perm, :][:nfew, :]
# few_labels = train_labels[perm][:nfew]

# # Naive t-SNE without dimension reduction
# # time_start = time.time()
# # tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
# # tsne_results = tsne.fit_transform(few_examples)

# # print('t-SNE done! Time elapsed: {} seconds'.format(time.time() - time_start))

# # plt.figure()
# # plt.scatter(tsne_results[:, 0], tsne_results[:, 1], c=few_labels)

# # plt.colorbar()

# # t-SNE on the first 50 PCA components
# print("t-SNE on the first 50 PCA components")
# print("------------------------------------")
# pca_50 = PCA(n_components=50)
# pca_50_result = pca_50.fit_transform(few_examples)

# print("Keeping only {}% of the variance".format(np.sum(pca_50.explained_variance_ratio_)))

# tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
# tsne_pca_results = tsne.fit_transform(pca_50_result)

# plt.figure()
# plt.scatter(tsne_pca_results[:, 0], tsne_pca_results[:, 1], c=few_labels)

# plt.colorbar()


# def pca(x, p=None, return_u=False):
#     C = x.T @ x / (len(x) - 1) # Covariance matrix
#     eigenvals, eigenvecs = np.linalg.eig(C)

#     if p is None:
#         p = len(eigenvecs)

#     u = eigenvecs[:, :p].real

#     if return_u:
#         return x @ u, u
#     return x @ u

# vecs = pca(train_examples)

# # Plot the first and second PCA against the labels (using a cmap)
# plt.figure()
# plt.scatter(vecs[:, 0], vecs[:, 1], c=train_labels)

# plt.title('PCA 0 vs PCA 1')
# plt.xlabel('PCA 0')
# plt.ylabel('PCA 1')
# plt.colorbar()

# # Plot the norm of each descriptor against the labels
# plt.figure()
# plt.scatter(np.linalg.norm(train_examples, axis=1), train_labels)

# plt.title('Norm against label')
# plt.xlabel('norm')
# plt.ylabel('label')

plt.show()

# # Now retrieve every descriptor in the first cluster to see what's going on
# # First cluster PCA0 < 4.95:
# # 288 elements
# # indices from 36 to 95 inc, 276 to 311 inc, 360 to 371, 384 to 431
# # inc, 444 to 455, 480 to 491, 504 to 539, 552 to 563, 624 to 647, 720
# # to 731, 768-779, 840-851, 876-887
# # size of the indices clusters : 60, 36, 12, 12, 36 ...
# # Second cluster PCA0 < 5.05:
# # 252 elements
# # Third cluster PCA0 < 5.13:
# # 462 elements
# # Fourth cluster PCA0 > 5.13:
# # 198 elements

"""The final version of bond.py"""

import numpy as np
import pymatgen as mg
import math

from .hr import HrDatFile
from .win import WinFile

def unzip(l):
    if len(l) == 0:
        return [[], []]
    return [list(t) for t in zip(*l)]

def normalize(v):
    return v / np.linalg.norm(v)

class Descriptor:
    def __init__(self, path, vec_shape, max_atom_dist, max_bond_len):
        self.structure = mg.Structure.from_file(path)
        # A list like [("C", 5)] for example
        self.vec_shape = vec_shape
        # The maximum distance between an atom and the center of the
        # bond for the atom to be taken into account by the descriptor
        self.max_atom_dist = max_atom_dist
        # The maximum length of the bonds to be taken into account
        # The describe method will return None for bond length greater
        self.max_bond_len = max_bond_len

        # All the atoms in the structure must be found in the vector shape
        species_set = set(self.structure.species)
        vec_species = [specie for specie, num in vec_shape]
        for s in species_set:
            if not str(s) in vec_species:
                raise RuntimeError("The atom {} was not specified in the vec_shape argument".format(str(s)))

        # The descriptors will be of dimension 4 * dim + 1
        self.dim = sum(num for specie, num in vec_shape)

    def _get_nearest_sorted(self, center, excluding=None):
        """Returns the coordinates (without unit cell wrapping) of each site
        closer than max_atom_dist of the center. If there are not
        enough atoms, pad with infs. Also sort them according to
        vec_shape. Exclude all sites in the excluding iterable.
        """
        print("center = {}".format(center))
        sites, dists = unzip([xs for xs in self.structure.get_sites_in_sphere(center, self.max_atom_dist)
                              if not xs[0] in excluding]) # Exclude given sites

        coords = np.full((self.dim, 3), np.nan)

        # Fill the coords array
        shift = 0
        for specie_str, nmax in self.vec_shape:
            # every site of the given specie
            ssites, sdists= unzip([(s, d) for s, d in zip(sites, dists) if str(s.specie) == specie_str])
            scoords = np.row_stack([s.coords for s in ssites]) # get their coords
            scoords = scoords[np.argsort(sdists)] # sort
            if scoords.shape[0] > nmax: # take only the closest ones if necessary
                scoords = scoords[:nmax]
            coords[shift:shift+len(scoords)] = np.row_stack(scoords)
            shift += nmax

        return coords

    @staticmethod
    def _rotation_matrix(coordsi, coordsj, coords):
        e1 = normalize(coordsj - coordsi)

        e2 = np.array([0, 0, 0])
        scoords = coords.copy()
        while (np.cross(e1, e2) == 0).all():
            if len(scoords) == 0:
                raise RuntimeError("All nearest sites are aligned")
            
            idx = np.nanargmax(np.linalg.norm(scoords, axis=1))
            e2 = scoords[idx]
            e2 -= e2.dot(e1) * e1 # project onto <e1>^T
            np.delete(scoords, idx, axis=0)

        e2 = normalize(e2)
        e3 = np.cross(e1, e2)
        print("rotmat = \n{}".format(np.column_stack([e1, e2, e3])))

        return np.row_stack([e1, e2, e3]) # Build directly the inverse

    @staticmethod
    def _extract_radial(coord):
        if np.isnan(coord).any():
            return np.array([0, 0, 0, 0])
        R = np.linalg.norm(coord)
        return np.append(coord, [1]) / R

    def final_shape(self):
        """Returns the shape of the produced by self.describe"""
        nneighbors = sum([n for (specie, n) in self.vec_shape])
        return (1 + 4 * nneighbors,)
        
    def describe(self, i, j, R):
        """Returns a descriptor [array] for the bond from the site (i, 0) to
        the site (j, R). """
        R = np.array(R)
        sitei = self.structure[i]
        sitej = self.structure[j]
        if sitei.specie.Z < sitej.specie.Z:
            return self.describe(j, i, (-1) * R) # Make sure that the largest Z will always be first
        
        # The coords without unit cell wrapping
        coordsi = sitei.coords
        coordsj = sitej.coords + self.structure.lattice.get_cartesian_coords(R)

        center = 0.5 * (coordsi + coordsj)
        length = np.linalg.norm(coordsj - coordsi)
        if length > self.max_bond_len:
            return None # Skip it

        coords = self._get_nearest_sorted(center, excluding=[sitei, sitej])
        # Translation invariance
        coords -= center
        # Rotation invariance
        rotmat = Descriptor._rotation_matrix(coordsi, coordsj, coords)
        coords = np.row_stack([rotmat @ r for r in coords]) # Also add back the two bonded atoms
        # Eliminate the bond inversion symmetry
        # TODO do somthing less wasteful of computational resources, but for now it's good enough
        # TODO find a solution this does not seem to work at all
        # if coords[2, 0] < 0:
        #     print("Inverting bond")
        #     print("coords = {}".format(coords))
        #     return self.describe(j, i, (-1) * R)
        
        # Transform cartesian coords into (x, y, z, 1) / R
        coords = np.row_stack([Descriptor._extract_radial(r) for r in coords])


        return np.insert(coords, 0, length)

def possibleBonds(species):
    xs = []
    for i in range(len(species)):
        for j in range(i, len(species)):
            xs.append((species[i], species[j]))
    return xs

def build_dataset(poscar, hr_dat, vec_shape, max_atom_dist, max_bond_len, outdir="./"):
    """Generate the dataset and label for each type of interaction with
    the given poscar and hr_dat. I.e. for a carbon and nitrogen
    material, makes for NN, CC, and NC interactions.
    """
    d = Descriptor(poscar, vec_shape, max_atom_dist, max_bond_len)
    hr = HrDatFile(hr_dat)

    bonds = possibleBonds(["C"])
    bond = ("C", "C")
    descs = {}  # keys : ("N", "C")
    labels = {} # keys : ("N", "C")
    
    for R in hr.Hr.keys():
        # TODO hack for 2D systems : skip interaction between two layers
        if R[2] != 0:
            continue
        for m in range(hr.num_wann):
            for n in range(hr.num_wann):
                if R == (0, 0, 0) and n == m:
                    continue # TODO implement this case later
                current_descriptor = d.describe(m, n, R)
                if current_descriptor is not None:
                    print("Not skipped R={} n={} m={}".format(R, n, m))
                    if not (R, m, n) in descs.setdefault(bond, []):
                        descs[bond].append(current_descriptor)
                    else:
                        print("Warning : already here : bond={}, (R, m, n)={}".format(bond, (R, m, n)))
                    labels.setdefault(bond, []).append(hr.Hr[R][m, n]) # for now no SOC so just the real value

    # now for each bond type, flatten the dicts to arrays
    # for a (N, C) bond : labels.shape = (Nsamples, nN, nC)
    #                     descs.shape  = (Nsamples, descriptorDim)
    for bond in descs.keys():
        bstring = bond[0] + "-" + bond[1]
        arr = descs[bond]
        arr = np.row_stack(arr)
        np.save(outdir + "descs_{}.npy".format(bstring), arr)

        arr = labels[bond]
        arr = np.row_stack(arr).reshape((-1,))
        np.save(outdir + "labels_{}.npy".format(bstring), arr)

    return descs, labels

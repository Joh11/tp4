"""A script to read the spread of each WFs from the seedname.wout file
produced by Wannier90"""

import numpy as np

def get_spreads(filename="wannier90.wout"):
    "Returns a list of all the spreads for each WF"
    with open(filename) as f:
        lines = f.readlines()

    indices_final = [i for i, line in enumerate(lines) if "Final State" in line]
    if len(indices_final) != 1:
        raise RuntimeError("Multiple final states in {} : {}".format(filename, indices_final))

    index_final = indices_final[0]
    
    spreads = []
    i = index_final + 1

    def grab_spread(s):
        idx = s.index(')')
        return float(s[idx + 1:])

    while i < len(lines) and "WF centre and spread" in lines[i]:
        spreads.append(grab_spread(lines[i]))
        i += 1

    return spreads

def are_spreads_localized(filename="wannier90.wout", threshold=2.):
    """Returns true if all the spreads are smaller than the given threshold. """
    spreads = np.array(get_spreads(filename))
    return np.all(spreads < threshold)
    

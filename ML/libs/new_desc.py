"""New version of the bond descriptor proposed by Q. Wu"""

import numpy as np
import pymatgen as mg
from math import sqrt
from pymatgen.io.vasp import Poscar # TODO Remove after testing
from functools import cmp_to_key

from .hr import HrDatFile
from .utils import *

epsilon = 1e-8

def generate_perms(xs, remove_singles=True, tol=1e-4):
    """From an increasing ordered list of numbers, returns the equivalence
    classes of the indices with respect to the equivalence i ~ j <=> xs[i]
    is close enough to xs[j]"""
    N = len(xs)
    perms = [[i] for i in range(N)]

    i = 0
    while i < len(perms) - 1:
        cur_class = perms[i]
        next_class = perms[i+1]

        cur_rep = cur_class[-1]
        next_rep = next_class[0]

        if np.abs(xs[cur_rep] - xs[next_rep]) < tol:
            # merge the two classes
            perms[i] = cur_class + next_class
            del perms[i+1]
        else:
            i += 1
    if remove_singles:
            perms = [x for x in perms if len(x) > 1]
    return perms

def fix_permutation(coords, perms):
    """Fix the permutation ambiguity in the given coords. To do this, sort
    using fuzzy lexical sort on the x, y, and z coordinates.

    Arguments:
    coords -- Nx3 array of coordinates
    perms  -- permutations (list of list of indices). See
              generate_perms for more infos.
    """
    tol = 1e-4
    for perm in perms:
        problematic_coords = coords[perm, :]
        N = len(problematic_coords)
        idcs = np.arange(N)

        def custom_cmp(i, j):
            x = problematic_coords[i]
            y = problematic_coords[j]

            if np.abs(x[0] - y[0]) > tol:
                return x[0] - y[0]
            if np.abs(x[1] - y[1]) > tol:
                return x[1] - y[1]
            if np.abs(x[2] - y[2]) > tol:
                return x[2] - y[2]
            print('Warning, sorting equivalent values')
            return x[2] - y[2]
        
        idcs = sorted(idcs, key=cmp_to_key(custom_cmp))
        coords[perm, :] = coords[perm, :][idcs, :]

    return coords

class Descriptor:
    def __init__(self, poscar, plane_radius, zdist, nneighbors, max_bond_len):
        """Construct a descriptor generator from the given POSCAR file. 

        Arguments:
        poscar          -- path string to the POSCAR file
        plane_radius    -- radius of neighborhood around an atom in the xy plane
        zdist           -- distance of neighborhood around an atom in z axis (cylinder)
        nneighbors      -- maximum number of neighbors per atom
        max_bond_len    -- maximum length of the bond that can be decribe. Return
                           None if condition not meet
        """
        self.struct = mg.Structure.from_file(poscar)
        self.plane_radius = plane_radius
        self.zdist = zdist
        self.nneighbors = nneighbors
        self.max_bond_len = max_bond_len

        if 2 * plane_radius < max_bond_len:
            print("Warning : neighborhoods do not overlap")
            
    def _coords_nearest_sorted(self, coords, gen_perms=False):
        """Returns a list of all atoms in the neighborhood of the atom in (i,
        R). This list has length nneighbors, padding with infs if
        necessary.
        
        Arguments:
        i         -- index of the atom
        R         -- lattice vector to the unit cell of the atom
        gen_perms -- returns also the permutation group. 

        About the gen_perms argument: when there are two sites at the
        same distance to the center, it is ambiguous. If gen_perms is
        set to True, the second part of the return tuple will consist
        of a list of lists. Each sublist represents a cluster of
        indices of sites that can be swapped without breaking the
        ordering of the list.
        """
        sites, dists = unzip([xs for xs in get_sites_in_cylinder(self.struct, coords,
                                                                 self.plane_radius, self.zdist)])

        idx_sort = np.argsort(dists)
        sites = np.array(sites)[idx_sort]
        dists = np.array(dists)[idx_sort]

        coords = np.row_stack([site.coords for site in sites])

        if len(coords) > self.nneighbors:
            coords = coords[:self.nneighbors]
        elif len(coords) < self.nneighbors:
            coords_padded = np.full((self.nneighbors, 3), np.inf)
            coords_padded[:len(coords), :] = coords
            coords = coords_padded

        if gen_perms:
            self.dists = dists # TODO remove later
            return coords, generate_perms(dists)
        return coords

    def _get_rotation_mat(self, coordsi, coordsj, zdir=[0,0,1]):
        """Returns the rotation matrix to the canonical frame of
        reference. Uses the bond direction as the x axis, and the
        direction to the other layer as the z axis. 

        Arguments: 
        coordsi -- coordinates of the first atom
        coordsj -- coordinates of the second atom
        zdir    -- unit vector toward the z direction
        """
        x = (coordsj - coordsi)
        x = x / np.linalg.norm(x)
        
        z = np.array(zdir)
        y = np.cross(z, x)

        return np.row_stack([x, y, z])

    def _invert_bond_p(self, coordsi, coordsj, neighbors):
        """Returns True if the bond should be inverted. Take the atom closest
        to the center of mass of the neighborhood as the first bonding atom.
        
        Arguments:
        coordsi   -- the coordinates of the first bonding atom
        coordsj   -- the coordinates of the second bonding atom
        neighbors -- array of coordinates of neighbors
        """
        neighbors = np.row_stack([n for n in neighbors if not np.isinf(n).any()])
        # remove duplicates
        nodup = []
        for n in neighbors:
            if np.array([np.linalg.norm(n-nd)<1e-4 for nd in nodup]).any():
                continue
            nodup.append(n)
        neighbors = np.row_stack(nodup)
        
        center = (coordsi + coordsj) / 2
        vec = coordsj - coordsi ; vec = vec / np.linalg.norm(vec)

        neighbors -= center
        dots = (neighbors * vec).sum(axis=1)
        total = dots.sum()
        print('total={}'.format(total))
        
        if np.abs(total) < 1e-4:
            print('Symmetry assumed. Bond direction should not matter')
            return False
        if total > 0:
            return True
        return False
            
    def describe(self, i, j, R, zdir=[0,0,1]):
        """Describe the bond from the ith atom of the unit cell to the
        jth of the R cell. 

        Arguments: 
        i -- index of the first atom
        j -- index of the second atom
        R -- lattice vector to the unit cell of the second atom
        """
        R = np.array(R)
        
        coordsi = self.struct[i].coords
        coordsj = self.struct[j].coords + self.struct.lattice.get_cartesian_coords(R)

        # Translation invariance
        center = (coordsi + coordsj) / 2
        length = np.linalg.norm(coordsi - coordsj)

        if length > self.max_bond_len:
            print('Bond too long : {} A'.format(length))
            return None

        print('Describing {} {} {}'.format(i, j, R))
        
        # Rotation invariance
        # TODO change the zdir accordingly
        mat = self._get_rotation_mat(coordsi, coordsj, zdir=zdir)
        
        neighborsi, permsi = self._coords_nearest_sorted(coordsi, gen_perms=True)
        neighborsj, permsj = self._coords_nearest_sorted(coordsj, gen_perms=True)

        if self._invert_bond_p(coordsi, coordsj, np.concatenate((neighborsi, neighborsj), axis=0)):
            print('Inverting bond ... {} {} {}'.format(i, j, R))
            # swap everything
            coordsi, coordsj = coordsj, coordsi
            neighborsi, neighborsj = neighborsj, neighborsi
            permsi, permsj = permsj, permsi
            # TODO check if this works
            # print(mat)
            mat[0, :] *= -1
            mat[1, :] *= -1
            # print(mat)
        
        self.permsi, self.permsj = permsi, permsj # TODO temporary
        
        # Change the frame of reference
        neighborsi = np.row_stack([mat @ row for row in (neighborsi - center)])
        neighborsj = np.row_stack([mat @ row for row in (neighborsj - center)])

        # Fix the ambiguity of distance (with permutations)
        neighborsi = fix_permutation(neighborsi, permsi)
        neighborsj = fix_permutation(neighborsj, permsj)
        
        # Apply the radial-angular transformation
        neighborsi = np.row_stack([cart_to_rad(row) for row in neighborsi])
        neighborsj = np.row_stack([cart_to_rad(row) for row in neighborsj])
        
        # Merge and return
        return np.concatenate((neighborsi, neighborsj)).reshape((-1,))

def undescribe(desc):
    """Returns an array of coordinates of atoms from the given descriptor. 

    Arguments:
    desc -- the 2*N*4 1D array, where N is the number of neighbors per atom

    Returns:
    desc       -- the (n, 3) array of cartesian coordinates
    coordsi    -- the coordinates of the first bonding atom
    coordsj    -- the coordinates of the second bonding atom
    """
    if not (len(desc.shape) == 1) or not (len(desc) % 8 == 0):
        raise RuntimeError("Wrong shape for the descriptor")

    desc = np.row_stack([rad_to_cart(row) for row in desc.reshape((-1, 4))])
    Natoms = len(desc)
    i, j = 0, Natoms // 2
    
    idcs_and_desc = np.row_stack([(idx, row) for idx, row in enumerate(desc)
                                  if not (row == np.inf).any()])
    idcs, desc = unzip(idcs_and_desc)
    idcs = np.array(idcs)

    i = np.argwhere(idcs == i)[0][0]
    j = np.argwhere(idcs == j)[0][0]

    coordsi = desc[i]
    coordsj = desc[j]

    desc = np.delete(desc, [i, j], axis=0)

    # TODO Workaround : if an atom is too close to site i or j we remove it
    desc = np.row_stack([row for idx, row in enumerate(desc)
                         if np.linalg.norm(row - coordsi) > epsilon
                         and np.linalg.norm(row - coordsj) > epsilon])
    return desc, coordsi, coordsj

def build_nn_dataset(poscar, hr_dat, plane_radius, zdist, nneighbors, max_bond_len, outdir="./", prefix="", save_files=True):
    """Generate the dataset and labels for nearest neighbors bond. For now
    only works with single specie materials.
    
    Arguments:
    poscar                                        -- path to POSCAR file
    hr_dat                                        -- path to seedname_hr.dat file
    plane_radius, zdist, nneighbors, max_bond_len -- see Descriptor for more details
    outdir                                        -- directory for data.npy and labels.npy
    prefix                                        -- prepend this to the name of the files
    save_files                                    -- if True save the files in outdir
    """
    d = Descriptor(poscar, plane_radius, zdist, nneighbors, max_bond_len)
    hr = HrDatFile(hr_dat)

    descs = []
    labels = []

    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            R = (x, y, 0)
            for i in [0, 1]:
                for j in [0, 1]:
                    if x == 0 and y == 0 and i == j:
                        continue # Remove self interaction
                    dd = d.describe(i, j, R, [0, 0, 1])
                    if dd is not None:
                        descs.append(dd)
                        # TODO check if this works
                        labels.append(hr.Hr[R][i, j])
            for i in [2, 3]:
                for j in [2, 3]:
                    if x == 0 and y == 0 and i == j:
                        continue # Remove self interaction
                    dd = d.describe(i, j, R, [0, 0, -1])
                    if dd is not None:
                        descs.append(dd)
                        # TODO check if this works
                        labels.append(hr.Hr[R][i, j])

    # Save everything
    descs = np.row_stack(descs)
    labels = np.row_stack(labels)
    if save_files:
        np.save(outdir + prefix + "data.npy", descs)
        np.save(outdir + prefix + "labels.npy", labels)

    return descs, labels

def build_dataset(poscar, hr_dat, plane_radius, zdist, nneighbors, max_bond_len, outdir="./", prefix="", save_files=True):
    """Generate the dataset and labels for each bond. For now only works
    with single specie materials. 
    
    Arguments:
    poscar                                        -- path to POSCAR file
    hr_dat                                        -- path to seedname_hr.dat file
    plane_radius, zdist, nneighbors, max_bond_len -- see Descriptor for more details
    outdir                                        -- directory for data.npy and labels.npy
    prefix                                        -- prepend this to the name of the files
    save_files                                    -- if True save the files in outdir"""
    d = Descriptor(poscar, plane_radius, zdist, nneighbors, max_bond_len)
    hr = HrDatFile(hr_dat)

    descs = []
    labels = []
    
    for R in hr.Hr.keys():
        # Hack for 2D systems : skip interaction between two layers
        if R[2] != 0:
            continue
        for m in range(hr.num_wann):
            for n in range(hr.num_wann):
                if R == (0, 0, 0) and n == m:
                    continue # TODO implement this case later
                current_descriptor = d.describe(m, n, R)
                if current_descriptor is None:
                    continue # Skip it
                print("Not skipped m={} n={} ({} {} {})".format(m, n, R[0], R[1], R[2]))
                if current_descriptor.max() > 1e3:
                    print("Wrong descriptor")
                descs.append(current_descriptor)
                labels.append(hr.Hr[R][m, n])

    # Save everything
    descs = np.row_stack(descs)
    labels = np.row_stack(labels)
    if save_files:
        np.save(outdir + prefix + "data.npy", descs)
        np.save(outdir + prefix + "labels.npy", labels)

    return descs, labels

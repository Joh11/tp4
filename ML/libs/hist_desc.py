"""Test of an histogram descriptor. For now, only do it with the radial information"""

import numpy as np
import pymatgen as mg

from .hr import HrDatFile
from .utils import *

class HistDescriptor:
    def __init__(self, poscar, max_bond_len, radius, dim):
        self.struct = mg.Structure.from_file(poscar)
        self.max_bond_len = max_bond_len
        self.radius = radius
        self.dim = dim
        self.delta = radius / dim

    def describe(self, i, j, R):
        R = np.array(R)
        
        coordsi = self.struct[i].coords
        coordsj = self.struct[j].coords + self.struct.lattice.get_cartesian_coords(R)

        # Translation invariance
        center = (coordsi + coordsj) / 2
        length = np.linalg.norm(coordsi - coordsj)

        if length > self.max_bond_len:
            print('Bond too long : {} A'.format(length))
            return None

        desc = np.zeros((self.dim,))
        sites, dists = unzip([xs for xs in self.struct.get_sites_in_sphere(center, self.radius)])
        for d in dists:
            idx = int(d / self.delta)
            if idx < self.dim:
                desc[idx] += 1
        return desc
        
def build_nn_dataset(poscar, hr_dat, max_bond_len, radius, dim, outdir="./", prefix="", save_files=True):
    d = HistDescriptor(poscar, max_bond_len, radius, dim)
    hr = HrDatFile(hr_dat)
    
    descs = []
    labels = []

    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            R = (x, y, 0)
            for i in [0, 1]:
                for j in [0, 1]:
                    if x == 0 and y == 0 and i == j:
                        continue # Remove self interaction
                    dd = d.describe(i, j, R)
                    if dd is not None:
                        descs.append(dd)
                        # TODO check if this works
                        labels.append(hr.Hr[R][i, j])
            for i in [2, 3]:
                for j in [2, 3]:
                    if x == 0 and y == 0 and i == j:
                        continue # Remove self interaction
                    dd = d.describe(i, j, R)
                    if dd is not None:
                        descs.append(dd)
                        # TODO check if this works
                        labels.append(hr.Hr[R][i, j])

    # Save everything
    descs = np.row_stack(descs)
    labels = np.row_stack(labels)
    if save_files:
        np.save(outdir + prefix + "data.npy", descs)
        np.save(outdir + prefix + "labels.npy", labels)

    return descs, labels

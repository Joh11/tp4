"""New version from scratch of the descriptor"""

import numpy as np
import pymatgen as mg

class Descriptor:
    def __init__(self, poscar, neighbor_radius, nneighbors, max_bond_len):
        self.struct = mg.Structure.from_file(poscar)
        self.neighbor_radius = neighbor_radius
        self.nneighbors = nneighbors
        self.max_bond_len = max_bond_len

    def describe(self, i, j, R):
        R = np.array(R)
        
        coordsi = self.struct[i].coords
        coordsj = self.struct[j].coords + self.struct.lattice.get_cartesian_coords(R)

        center = (coordsi + coordsj) / 2
        length = np.linalg.norm(coordsi - coordsj)

        if length > max_bond_len:
            print("Skipped because bond too large")
            return None

        coords = coords_nearest_sorted(center)

def rad_to_cart(xs):
    """1/r x/r y/r z/r -> x y z"""
    return xs[1:] / xs[0]
        
def undescribe(desc):
    """Returns an array of coordinates of atoms, and the length of the bond"""
    if len(desc) % 4 != 1:
        raise RuntimeError("Invalid descriptor size : {}".format(len(desc)))
    
    length = desc[0]
    
    desc = desc[1:]
    desc = desc.reshape((-1, 4))
    desc = desc[desc[:, 0] != 0, :] # Remove empty slots
    natoms = len(desc)
    print(natoms)
    desc = np.apply_along_axis(rad_to_cart, 1, desc)

    # Now construct the structure
    Catom = mg.Element("C")
    Oatom = mg.Element("O") # Use oxygen to represent bonding atoms (they're carbon in reality ofc)

    lattice = mg.Lattice([[10 * length, 0, 0],
                          [0, 10 * length, 0],
                          [0, 0, 10 * length]])

    bondPos = np.array([[length / 2., 0, 0], [-length / 2., 0, 0]])
    print(bondPos)
    print(desc)
    print(2 * [Oatom] + natoms * [Catom])

    structure = mg.Structure(lattice, 2 * [Oatom] + natoms * [Catom], 5 * length * np.array([1., 1., 1.]) + np.concatenate((bondPos, desc)), coords_are_cartesian=True)
    return structure
    

"""A script to read and parse the content of a seedname.win file"""

import numpy as np

class WinFile:
    def __init__(self, path):
        with open(path, "r") as f:
            lines = f.readlines()

        self.atoms = []
        for k, line in enumerate(lines):
            if "atoms_cart" in line.lower():
                for line in lines[k+1:]:
                    if "atoms_cart" in line:
                        break
                    self.atoms.append(line.split(" ")[0])
                # self.natoms = int(lines[k + 1])
                # for j in range(self.natoms):
                #     self.atoms.append(lines[k+3+j].split(" ")[0])
                break

        if self.atoms == []:
            raise RuntimeError("No atoms in the unit cell, or no atom_positions line block found")

        self.species = list(set(self.atoms))
        self.nspecies = len(self.species)

        self.projectors = {}
        for k, line in enumerate(lines):
            if "projections" in line.lower():
                # self.nprojectors = [int(y) for y in [x for x in lines[k + 1].split(" ")
                #                                      if x != ""][:self.nspecies]]

                for line in lines[k+1:]:
                    if "projections" in line:
                        break
                    tokens = line[:-1].replace(":", " ").split(" ") # Remove the \n, : and tokenize
                    tokens = [x for x in tokens if x != ""] # Remove empty tokens
                    self.projectors[tokens[0]] = tokens[1:]
                # for j, p in enumerate(self.nprojectors):
                #     tokens = [x for x in lines[k+2+j].split(" ")
                #               if x != ""][:p + 1]
                #     self.projectors[tokens[0]] = tokens[1:]
                break
        if len(self.projectors) != self.nspecies:
            raise RuntimeError("Problem")

        # list every atom and its orbitals to simplify for later
        list_of_orbs = [(atom, self.projectors[atom]) for atom in self.atoms]
        list_of_species = [len(ls) * [specie] for specie, ls in list_of_orbs]
        list_of_orbs = [projectors for atom, projectors in list_of_orbs]
        self.projector_list = [item for sublist in list_of_orbs for item in sublist] # flatten
        self.species_list = [item for sublist in list_of_species for item in sublist] # flatten

    def geti(self, m):
        acc = 0
        for k, atom in enumerate(self.atoms):
            nproj = len(self.projectors[atom])
            if acc + nproj > m: # if the band index is in this atom
                p = m - acc
                return k, p, atom, self.projectors[atom][p]
            acc += nproj

        raise RuntimeError("i too big for the win file (i={})".format(i))

    def pnum(self, specie):
        return len(self.projectors[specie])

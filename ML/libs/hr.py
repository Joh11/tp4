"""A script to read and parse the content of a seedname_hr.dat file"""

import numpy as np
import math

class HrDatFile:
    """Opens and parse the given _hr.dat file. 

    Attributes : 
    num_wann   -- number of Wannier functions per unit cell
    nrpts      -- number of Wigner Seitz grid-points
    degeneracy -- array of the degeneracy of each Wigner Seitz grid-point
    Hr         -- dictionary with key (Rx, Ry, Rz) the vector R and value 
                  Hmn(R) the real part of the hopping matrix. Dimension of
                  the matrix : num_wann * num_wann
    Hr         -- dictionary with key (Rx, Ry, Rz) the vector R and value 
                  Hmn(R) the imaginary part of the hopping matrix. Dimension of
                  the matrix : num_wann * num_wann
    """
    def __init__(self, path):
        """Parse the file given. 

        Argument : 
        path -- string, path to the file. It must respect the hr.dat format 
                from Wannier90"""
        with open(path, "r") as f:
            lines = f.readlines()

        self.num_wann = int(lines[1])
        self.nrpts = int(lines[2]) # n of Wigner Seitz grid-points

        nlines = math.ceil(self.nrpts / 15)
        deglines = "\n".join(lines[3:3+nlines])
        self.degeneracy = np.array([int(token) for token in deglines.split()])
        
        self.Hr = {} # Elements will be num_wann * num_wann matrices
        self.Hi = {} # Dicts indexed by R = (Ra, Rb, Rc)

        # now fill the dictionaries
        Hlines = lines[3+nlines:]
        for line in Hlines:
            Ra, Rb, Rc, m, n = [int(val) for val in line.split()[:-2]]
            Hr, Hi = [float(val) for val in line.split()[-2:]]

            R = (Ra, Rb, Rc)

            # m-1 as the band index starts at 1 in hr.dat
            zeroMatrix = np.full((self.num_wann, self.num_wann), np.nan)
            self.Hr.setdefault(R, zeroMatrix.copy())[m-1, n-1] = Hr
            self.Hi.setdefault(R, zeroMatrix.copy())[m-1, n-1] = Hi

        def getRs(self):
            return list(self.Hr.keys()) # both Hr and Hi have the same keys

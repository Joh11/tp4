"""Given a descriptor, POSCARs and hrdat files, make a dataset"""

from desc import Descriptor
from hr import HrDatFile
import numpy as np

def generate_dataset(poscar, hrdat):
    d = Descriptor(poscar)
    print("Loaded {} ...".format(poscar))
    hrfile = HrDatFile(hrdat)
    print("Loaded {} ...".format(hrdat))

    nwann = hrfile.num_wann
    print("Number of Wannier functions per unit cell = {}".format(nwann))
    Hr = hrfile.Hr

    data, labels = [], []
    for R in Hr.keys():
        print("Starting R = {}".format(R))
        for i in range(nwann):
            for j in range(nwann):
                if i == j:
                    continue
                
                desc = d.describe(i, j, R)
                if desc is not None:
                    data.append(desc)
                    labels.append(Hr[R][i, j])

    np.save("data.npy", np.array(data))
    np.save("labels.npy", np.array(labels))

import numpy as np
import pymatgen as mg
from math import sqrt

epsilon = 1e-8

def unzip(l):
    """Turns a list of couples into two lists. """
    if len(l) == 0:
        return [[], []]
    return [list(t) for t in zip(*l)]

def rad_to_cart(xs):
    """Build a cartesian representation from the radial - angular
    representation. The null vector will be mapped to infs.
    
    Argument:
    xs -- a 4D vector like [1/r, x/r, y/r, z/r]"""
    if xs[0] == 0:
        return np.full((3,), np.inf)
    return xs[1:] / xs[0]

def cart_to_rad(xs):
    """Build a radial - angular representation from the cartesian
    representation. The null vector and infinite vectorswill be mapped
    to zero.

    Argument:
    xs -- a 3D vector to convert.
    """
    r = np.linalg.norm(xs)
    if r == np.inf or np.isnan(xs).any():
        return np.zeros((4,))
    if r < epsilon: # Send 0 to 1 0 0 0 à la homogeneous coordinates
        return np.array([1., 0., 0., 0.])
    return np.insert(xs, 0, 1) / r

def cylinder_distance(x, y):
    """Return the xy plane and z distance between the two points. 

    Arguments:
    x, y -- (3x1 arrays) Cartesian coordinates of each point

    Returns:
    (r, d) with r the xy distance and d the z one. """
    dr = y - x
    r = sqrt(dr[0] ** 2 + dr[1] ** 2)
    d = abs(dr[2])
    
    return r, d

def in_cylinder(y, x0, r, d):
    """Returns True if the point y is in the cylinder centered in x0, of
    radius r and height/2 d."""
    rp, dp = cylinder_distance(x0, y)
    return rp <= r and dp <= d

def get_sites_in_cylinder(struct, pt, r, d, include_index=False, include_image=False):
    """Find all sites within a cylinder from a point. 
    
    Arguments:
    struct        -- pymatgen Structure
    pt            -- (3x1 array): cartesian coordinates of center of cylinder
    r             -- xy plane radius of the cylinder
    d             -- maximum distance from center in the z direction (height = 2*d)
    include_index -- whether the non-supercell site index is included in the returned data
    include_image -- whether to include the supercell image is included in the returned data

    Returns:
    [(site, dist, [index], [image]) ...] since most of the time, subsequent processing requires the distance.
    """
    # First make a big sphere including the cylinder
    rmax = sqrt(r**2 + d**2)
    neighbors = struct.get_sites_in_sphere(pt, rmax, include_index, include_image)
    # Then filter sites in the cylinder
    neighbors = [n for n in neighbors if in_cylinder(n[0].coords, pt, r, d)]
    return neighbors

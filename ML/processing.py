import numpy as np
import pymatgen as mg
from pymatgen.io.vasp import Poscar
import matplotlib.pyplot as plt

from libs.hr import HrDatFile
from libs.new_desc import build_dataset, Descriptor, undescribe, rad_to_cart

def coords_to_poscar(coords, coordsi, coordsj, poscarfile):
    """Takes an array of coordinates and saves the corresponding POSCAR file into the given file.

    Arguments:
    coords     -- Nx3 array of the coordinates of N atoms
    poscarfile -- target file to write"""
    N = len(coords)
    species = N * ['C'] + ['O', 'N']
    coords = np.concatenate((coords, np.row_stack([coordsi, coordsj])), axis=0)
    structure = mg.Molecule(species, coords).get_boxed_structure(10, 10, 10)
    Poscar(structure).write_file(poscarfile)

def desc_to_poscar(desc, poscarfile):
    """Take a descriptor and save the corresponding POSCAR file into the given file. 

    Arguments:
    desc       -- 2*N*4 1D array, descriptor for a bond with N-1 neighbors per atom
    poscarfile -- target file to write"""
    if desc is None:
        return
    coords, coordsi, coordsj = undescribe(desc)
    coords_to_poscar(coords, coordsi, coordsj, poscarfile)

def desc_to_coords(desc):
    desc = desc.reshape((-1, 4))
    coords = np.row_stack([rad_to_cart(row) for row in desc])
    return coords

def describe_all_nn(d):
    dds = []
    for i in [0, 1]:
        for j in [0, 1]:
            for x in [-1, 0, 1]:
                for y in [-1, 0, 1]:
                    if x == 0 and y == 0 and i == j:
                        continue
                    dd = d.describe(i, j, [x, y, 0], [0, 0, 1])
                    if dd is not None:
                        dds.append(dd)
                        desc_to_poscar(dd, 'poscar_{}_{}_{}.vasp'.format(i, j, [x, y, 0]))

    for i in [2, 3]:
        for j in [2, 3]:
            for x in [-1, 0, 1]:
                for y in [-1, 0, 1]:
                    if x == 0 and y == 0 and i == j:
                        continue
                    dd = d.describe(i, j, [x, y, 0], [0, 0, -1])
                    if dd is not None:
                        dds.append(dd)
                        desc_to_poscar(dd, 'poscar_{}_{}_{}.vasp'.format(i, j, [x, y, 0]))
    return dds

def check_all_eqs(dds, tol=1e-5):
    dists = np.zeros((len(dds), len(dds)))
    for i, dd1 in enumerate(dds):
        for j, dd2 in enumerate(dds):
            dists[i, j] = np.linalg.norm(dd1 - dd2)
    return (dists < tol)
                        
# Plot the magnitude of the hopping parameter as a function of distance
idcs = 2, 9
# hrdat = HrDatFile("data/bilayer_{}_{}_hr.dat".format(idcs[0], idcs[1]))
# bilayer = mg.Structure.from_file("data/POSCAR_bilayer_{}_{}".format(idcs[0], idcs[1]))

# hr = hrdat.Hr # real part of the hopping parameter

# dists = []
# hparams = []
# for R in hr.keys():
#     hrR = hr[R]
#     for i in range(hrdat.num_wann):
#         for j in range(hrdat.num_wann):
#             dists.append(bilayer.get_distance(i, j, R))
#             hparams.append(hrR[i, j])

# dists = np.array([dists])
# hparams = np.array([hparams])
            
# hist, bins = np.histogram(dists, bins=100)
# plt.figure()
# plt.hist(hist, bins = bins)
# plt.title("Distribution of the distance between Wannier functions ({}_{})".format(idcs[0], idcs[1]))
# plt.xlabel("Distance [Å]")
# plt.ylabel("Number of points")

# mask = dists < 100
# plt.figure()
# plt.scatter(dists[mask], hparams[mask])
# plt.title("Hopping strength as a function of distance ({}_{})".format(idcs[0], idcs[1]))
# plt.xlabel("Distance [A]")
# plt.ylabel("Hopping parameter [eV]")


# plt.show()

# Constants
plane_radius = 2
zdist = 4
nneighbors = 15
max_bond_len = 1.5

hrpath = "data/bilayer_{}_{}_hr.dat".format(idcs[0], idcs[1])
poscarpath = "data/POSCAR_bilayer_{}_{}".format(idcs[0], idcs[1])
winpath = "data/bilayer_{}_{}.win".format(idcs[0], idcs[1])
# descs, labels = build_dataset(poscarpath, hrpath, plane_radius, zdist, nneighbors, max_bond_len)
d = Descriptor(poscarpath, plane_radius, zdist, nneighbors, max_bond_len)

i, j = 0, 1
R = [0, -1, 0]
# i, j = 0, 1
# R = [0, -1, 0]
# i, j = 1, 0
# R = [1, 1, 0]
# dd = d.describe(i, j, R)
# desc_to_poscar(dd, 'poscar_{}_{}_{}.vasp'.format(i, j, R))
dds = describe_all_nn(d)

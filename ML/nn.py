#!/usr/bin/env python3
"""Here things are getting serious [or probably not]"""

import numpy as np
import tensorflow as tf
import h5py as h5
import datetime

# Import data
f = h5.File('dataset_june5.hdf5', 'r')
train_examples, train_labels = f['data'][:], f['labels'][:]

batch_size = 133
shuffle_buffer_size = 1200

mean = train_examples.mean(axis=0)
std = train_examples.std(axis=0)

# print("mean = {}".format(mean))
# print("std = {}".format(std))

# keep = 10
# train_examples = train_examples[:keep]
# train_labels = train_labels[:keep]

train_examples = (train_examples - mean) / (1e-6 + std)
# train_examples = pca(train_examples, p=50)
train_labels = (train_labels - np.mean(train_labels)) / np.std(train_labels)

train_dataset = tf.data.Dataset.from_tensor_slices((train_examples, train_labels))
train_dataset = train_dataset.shuffle(shuffle_buffer_size).batch(batch_size)

# Tensorboard stuff
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model = tf.keras.Sequential([
    tf.keras.layers.Dense(100, activation='relu'),
    tf.keras.layers.Dense(64, activation='relu'),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(16, activation='relu'),
    tf.keras.layers.Dense(1, activation='relu')])

model.compile(optimizer=tf.keras.optimizers.Adam(),
              loss=tf.keras.losses.MSE,
              metrics=['MSE'])

model.fit(train_dataset, epochs=100, callbacks=[tensorboard_callback])

#!/usr/bin/env python3

"""A script to generate datasets for the NN quickly from the POSCAR
and hrdat files"""

import numpy as np
import h5py as h5

import libs.new_desc as desc
# import libs.hist_desc as desc

# TODO remove later
from importlib import reload
reload(desc)

# Parameters
plane_radius = 2 # Take only nearest neighbors for now
zdist = 4 # interlayer distance should be 3.35 Ax
nneighbors = 15
max_bond_len = 1.5 # Take only nearest neighbors for now

# For histogram descriptor
# max_bond_len = 1.5 # Take only nearest neighbors for now
# dim = 100
# radius = 5

output_file = "dataset_june5.hdf5"
comment = """All data. Only nearest neighbor coupling (bond length < 1.5 A)"""

# Generate whole dataset
shifts = np.arange(100)

all_descs, all_labels = [], []
for i in shifts:
    for j in shifts:
        print("Starting bilayer_{}_{}".format(i, j))
        poscar = "data/june5/poscar_{}_{}.vasp".format(i, j)
        hr_dat = "data/june5/wannier90_{}_{}_hr.dat".format(i, j)
        # Bond of up to 6A, cf notes.org with Quansheng's remarks
        descs, labels = desc.build_nn_dataset(poscar, hr_dat,
                                              plane_radius, zdist, nneighbors,
                                              max_bond_len, save_files=False)
        # descs, labels = desc.build_nn_dataset(poscar, hr_dat, max_bond_len,
        #                                       radius, dim, save_files=False)
        all_descs.append(descs)
        all_labels.append(labels)
        
all_descs = np.concatenate(all_descs, axis=0)
all_labels = np.concatenate(all_labels, axis=0)

# Save everything in the hdf5 file
with h5.File(output_file, 'w') as f:
    f['data'] = all_descs
    f['labels'] = all_labels
    # Metadata
    f.attrs['comment'] = comment
    # For usual descriptor
    f.attrs['plane_radius'] = plane_radius
    f.attrs['zdist'] = zdist
    f.attrs['nneighbors'] = nneighbors
    f.attrs['max_bond_len'] = max_bond_len

    # For histogram descriptor
    # f.attrs['max_bond_len'] = max_bond_len
    # f.attrs['dim'] = dim
    # f.attrs['radius'] = radius
